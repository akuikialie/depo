CREATE TABLE `city` (
  `id_city` int(255) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(200) NOT NULL,
  `add_time` datetime NOT NULL,
  PRIMARY KEY (`id_city`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
