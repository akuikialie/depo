<?php echo $this->load->view('backend/inc_header_v') ?>
<?php echo $this->load->view('backend/inc_top_v') ?>
<div class="row">
    <div class="wrapper_content">
        <?php echo $this->load->view('backend/inc_sidebar_v') ?>
        <div id="content" class="nine columns">
            <div class="row">
                <div class="twelve columns box_gray box_dashboard box_corner">
                    <h2>Depo</h2>
                    <div class="divider"></div>
                    <table class='striped rounded'>
                        <thead>
                            <tr>
                                <th width="10%">No</th>
                                <th width="20%">Username</th>
                                <th width="20%">Name</th>
                                <th width="20%">Email</th>
								<th width="20%">Depo ID</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            if (!empty($datas))
                            {
                                foreach ($datas as $data)
                                {
                                    $edit = "<a href=" . base_url('users/view/' . str_replace(" ", "_",$data['uid'])) . ">View</a>";
                                    $delete = "<a href=" . base_url('users/delete/' . str_replace(" ", "_",$data['uid'])). ">Delete</a>";

                                    $action = $edit . " " . $delete;
                                    echo "<tr>" .
                                    "<td>" . $no . "</td>" .
                                    "<td>" . $data['unique_id'] . "</td>" .
                                    "<td>" . $data['name'] . "</td>" .
                                    "<td>" . $data['email'] . "</td>" .
									"<td>" . $data['depo_id'] . "</td>" .
                                    "<td>" . $action . "</td>" .
                                    "</tr>";
                                    $no++;
                                }
                            }
                            else
                            {
                                echo "<tr><td colspan='5'><div align='center'>No Users</div></td></tr>";
                            }
                            ?>							
                        </tbody>
                    </table>
                    <div class="clear"></div><br/>
                    <!--<div class="pagination" align="center"><?php echo $pagination ?></div>-->
                </div>
            </div>			
        </div>		
    </div>		
</div>
<?php echo $this->load->view('backend/inc_footer_v') ?>		