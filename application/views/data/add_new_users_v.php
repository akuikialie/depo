<?php echo $this->load->view('backend/inc_header_v') ?>
<?php echo $this->load->view('backend/inc_top_v') ?>
<div class="row">
    <div class="wrapper_content">
        <?php echo $this->load->view('backend/inc_sidebar_v') ?>
        <div id="content" class="nine columns">
            <div class="row">
                <div class="twelve columns box_gray box_dashboard box_corner">
                    <h2>Add Users</h2>					
                    <div class="divider"></div>
                    <div class="clear"></div>
                    <?php if (isset($error)) : ?>
                        <div class="danger  alert"><?php echo $error ?></div><br/>
                    <?php endif; ?>
                    <form name="f1" id="f1" class="field" method="post" action="<?php echo base_url('users/save') ?>" enctype="multipart/form-data">                        
                        <span>Name</span>
                        <input type="text"class="input" name="name" id="name" placeholder="Samsul Arifin"><br/><br/>
						<span>Username</span>
                        <input type="text"class="input" name="unique_id" id="company"placeholder="Arif45"><br/><br/>
						<span>Password</span>
                        <input type="password"class="input" name="encrypted_password" placeholder="password"><br/><br/>
						<span>Email</span>
                        <input type="email"class="input" name="email" id="company" placeholder="support@sucofindo.com"><br/><br/>
						<span>Depo ID</span>
                        <input type="text"class="input" name="depo_id" id="company" placeholder="DepoA"><br/><br/>
						<span>Level</span>
                        <input type="text"class="input" name="role_id" id="company" placeholder="Surveyor"><br/><br/>
                        
                        <input type ="hidden" id ="edit" name="edit" value="<?= isset($edit) ? $edit : ""; ?>">
                        <input type ="hidden" id ="id" name="id" value="<?= isset($datas['uid']) ? $datas['uid'] : ""; ?>">
                        <div class='clear'></div>	
                        <div class='clear'></div>						
                        <div class='divider'></div>						
                        <div class="medium secondary btn btn_full"><input type="submit" class="" value="Save"></div>
                    </form>
                </div>
                <div class='clear'></div>
            </div>			
        </div>		
    </div>		
</div>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/jquery.popupWindow.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/ckeditor/adapters/jquery.js"></script>	
<?php include('asset/js/ckeditor/ckinit.js.php'); ?>
<?php echo $this->load->view('backend/inc_footer_v') ?>		