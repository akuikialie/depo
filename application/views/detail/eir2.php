<body onLoad="javascript:window.print()">
 	<table width="835" border="0" align="center" style="font-size:10px;font-family:Tahoma, Geneva, sans-serif">
        <tr>
            <td><div align="left">KONSORSIUM PT.SUCOFINDO (Persero)</div></td>
            <td><div align="right"><?= date("d/m/y"); ?></div></td>
        </tr>
    </table>
 <div style="width:835px;border-bottom:1px solid #000;margin:0 auto;" align="center">
	 <div style="width:400px;border:1px solid #000;text-align:center;padding:0;margin:0;">
	 EQUIPMENT INTERCHANGE RECEIPT (EIR IN)
	 </div>
 </div>
<table width="835" border="0" align="center">
        <tr>
            <td width="194"><div style="font-family:Tahoma, Geneva, sans-serif; font-size:11px">Return date</div></td>
			<td>:</td>
            <td width="659"><div style="font-family:Tahoma, Geneva, sans-serif; font-size:11px"><?= date("d/m/y"); ?></div></td>
			<td width="194"><div style="font-family:Tahoma, Geneva, sans-serif; font-size:11px">Shipping Agent</div></td>
			<td>:</td>
            <td width="252"><div style="font-family:Tahoma, Geneva, sans-serif; font-size:11px"><?= $trans['line_id']; ?></div></td>
        </tr>
        <tr>
            <td><div style="font-family:Tahoma, Geneva, sans-serif; font-size:11px">Costumer </div></div></td>
			<td>:</td>
            <td><div style="font-family:Tahoma, Geneva, sans-serif; font-size:11px"><?= $transaction['customer_name']; ?></div></td>
			<td><div style="font-family:Tahoma, Geneva, sans-serif; font-size:11px">Trucking Co</div></td>
			<td>:</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><div style="font-family:Tahoma, Geneva, sans-serif; font-size:11px">Vessel</div></td>
			<td>:</td>
            <td><div style="font-family:Tahoma, Geneva, sans-serif; font-size:11px"><?= $trans['consignee']; ?></div></td>
			<td><div style="font-family:Tahoma, Geneva, sans-serif; font-size:11px">Vehicle No</div></td>
			<td>:</td>
            <td><div style="font-family:Tahoma, Geneva, sans-serif; font-size:11px"><?= $trans['in_truck_license_nbr']; ?></div></td>
        </tr>
    </table>
    <?php
    $index = 1;
    foreach ($equip as $v)
    {
        ?>
        <?php
        $index++;
    }
    ?>
    <table width="835" border="1" align="center">
        <tr>
            <td width="327" height="20" align="center">CONTAINER NUMBER</td>
			
            <td width="118" align="center">SIZE</td>
			
            <td width="118" align="center">TYPE</td>
			
            <td width="118" align="center">CONDITIONS</td>
			
            <td width="300" align="center">DATE MANUFACTURE</td>
        </tr>

        <tr>
            <td colspan="1"><div align="center" style="font-family:Tahoma, Geneva, sans-serif; font-size:11px"><?= $trans['eq_nbr']; ?></div></td>
            <td><div align="center"><?= $trans['eq_size']; ?></div></td>
            <td colspan="1"><div align="center" style="font-family:Tahoma, Geneva, sans-serif; font-size:11px"><?= $trans['eq_type']; ?></div></td>
            <td colspan="1">&nbsp;</td>
            <td colspan="1">&nbsp;</td>
        </tr>
        
		<tr height="44">
			<td colspan="3">Figures :<br />			
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
        <tr>
            <td colspan="3"><div style="font-family:Tahoma, Geneva, sans-serif; font-size:11px"><p>Detail :<br />
                        <?php
                        
                        foreach ($equip as $v)
                        {
							$filenaname="";
                            if ($v['bent_flag']=="Y")
                                $filenaname.=" bent,";
                            if ($v['Dentet_flag']=="Y")
                                $filenaname.=" dentet,";
                            if ($v['Leaking_flag']=="Y")
                                $filenaname.=" leaking,";
                            if ($v['PushIn_flag']=="Y")
                                $filenaname.=" pushin,";
                            if ($v['PushOut_flag']=="Y")
                                $filenaname.=" pushout,";
                            if ($v['Broke_flag']=="Y")
                                $filenaname.=" broke,";
                            if ($v['Hole_flag']=="Y")
                                $filenaname.=" hole,";
                            if ($v['Missing_flag']=="Y")
                                $filenaname.=" missing,";
                            if ($v['Cut_flag']=="Y")
                                $filenaname.=" cut,";
                            if ($v['Loose_flag']=="Y")
                                $filenaname.=" loose,";
                            if ($v['Tom_flag']=="Y")
                                $filenaname.=" tom,";
                            if ($v['Rusty_flag']=="Y")
                                $filenaname.=" rusty,";
                            echo $v['location'] . " " . $v['component']. " ".$filenaname;
                            echo "<br>";
                        }
                        ?>
                    </p>
					
					<p>&nbsp;</p>
					<p>&nbsp;</p>
					</div>
					</td>
            <td colspan="1"><div style="font-family:Tahoma, Geneva, sans-serif; font-size:11px">FOR RECEIVING DELIVERY CARRIER</p>
					
					<p>&nbsp;</p><p>&nbsp;</p></div></td>
            <td colspan="1"><div style="font-family:Tahoma, Geneva, sans-serif; font-size:11px">Printed / Scheduled by:<br> Konsorsium PT.SUCOFINDO</p>
					
					<p>&nbsp;</p><p>&nbsp;</p></div></td>
        </tr>
    </table>
</body>
