<?php

$eq_nbr = $eq_nbr;
define('FPDF_FONTPATH', 'fpdf17/font/');
require('fpdf17/fpdf.php');

class PDF extends FPDF {

    var $tablewidths;
    var $headerset;
    var $footerset;

    //Page header
    function Header()
    {
        //Logo
        //Arial bold 15
        $this->SetFont('Arial', 'B', 15);
        //pindah ke posisi ke tengah untuk membuat judul
        $this->Cell(80);
        //judul
        $this->Cell(30, 10, 'EQUIPMENT INTERCHANGE RECEIPT (EIR)', 0, 0, 'C');
        //pindah baris
        $this->Ln(20);
        //buat garis horisontal
        $this->Line(10, 25, 200, 25);
    }

    //Page Content
    function Content()
    {
        $this->SetFont('Times', '', 12);
        for ($i = 1; $i <= 40; $i++)
            $this->Cell(0, 10, 'line report ' . $i, 0, 1);
    }

    //Page footer
    function Footer()
    {
        //atur posisi 1.5 cm dari bawah
        $this->SetY(-15);
        //buat garis horizontal
        $this->Line(10, $this->GetY(), 200, $this->GetY());
        //Arial italic 9
        $this->SetFont('Arial', 'I', 9);
        //nomor halaman
        $this->Cell(-15, 10, 'Halaman ' . $this->PageNo() . ' dari {nb}', 0, 0, 'R');
    }

}

$pdf = new FPDF();
//Halaman 1
$pdf->_beginpage('P', 'A4');
$pdf->Image(BASE_ASSET . 'img/logo/asdekilogo.jpg', 18, 5);
$pdf->Image(BASE_ASSET . 'img/logo/scilogo.jpg', 160, 5);
$pdf->SetFont('Arial', 'B', 15);
/* Header */
$pdf->Cell(80);
$pdf->Cell(20, 0, 'EQUIPMENT INTERCHANGE RECEIPT (EIR)', 0, 0, 'C');
$pdf->Ln(2);
$pdf->SetFont('Arial', 'B', 15);
$pdf->Cell(80);
$pdf->Cell(20, 10, 'Incoming Container', 0, 0, 'C');
$pdf->Ln();
$pdf->Line(10, 25, 200, 25);

/* Container */
$pdf->SetFillColor(255, 0, 0);
$pdf->SetTextColor(255);
$pdf->SetDrawColor(128, 0, 0);
$pdf->SetFont('Arial', "B", 15);
$pdf->Ln();
$pdf->Cell(190, 7, 'Container Details', 1, 0, 'C', 1);
$pdf->Ln();
$pdf->SetX(10);
$pdf->SetFillColor(224, 235, 255);
$pdf->SetTextColor(0);
$pdf->SetFont('Arial', '', 14);
$pdf->Cell(50, 7, 'Container Code', 1, 0, 'L', 1);
$pdf->Cell(140, 7, '' . $eq_nbr . '', 1, 0, 'L', 1);
$pdf->Ln();
$pdf->Cell(50, 7, 'Tanggal', 1, 0, 'L', 1);
$pdf->Cell(140, 7, '' . $trans['in_time'] . '', 1, 0, 'L', 1);
$pdf->Ln();
$pdf->Cell(50, 7, 'Consignee', 1, 0, 'L', 1);
$pdf->Cell(140, 7, '' . $trans['consignee'] . '', 1, 0, 'L', 1);
$pdf->Ln();
$pdf->Cell(50, 7, 'Lines', 1, 0, 'L', 1);
$pdf->Cell(140, 7, '' . $trans['line_id'] . '', 1, 0, 'L', 1);
$pdf->Ln();
$pdf->Cell(50, 7, 'Depo', 1, 0, 'L', 1);
$pdf->Cell(140, 7, '' . $trans['depo_id'] . '', 1, 0, 'L', 1);
$pdf->Ln();
$pdf->Cell(50, 7, 'Shipping', 1, 0, 'L', 1);
$pdf->Cell(140, 7, '' . $trans['shipping_agent_name'] . '', 1, 0, 'L', 1);
$pdf->Ln();
$pdf->Cell(50, 7, 'B/L', 1, 0, 'L', 1);
$pdf->Cell(140, 7, '' . $transaction['doc_number'] . '', 1, 0, 'L', 1);
$pdf->Ln();
$pdf->SetFillColor(255, 0, 0);
$pdf->SetTextColor(255);
$pdf->SetDrawColor(128, 0, 0);
$pdf->SetFont('Arial', '', 14);
$pdf->Cell(190, 7, 'Detail Container', 1, 0, 'C', 1);
$pdf->Ln();
$pdf->SetX(10);
$pdf->SetFillColor(224, 235, 255);
$pdf->SetTextColor(0);
$pdf->SetFont('Arial', '', 14);
$pdf->Cell(25, 7, 'Origin', 1, 0, 'C', 1);
$pdf->Cell(25, 7, 'Destination', 1, 0, 'C', 1);
$pdf->Cell(35, 7, 'Iso Code', 1, 0, 'C', 1);
$pdf->Cell(20, 7, 'Size', 1, 0, 'C', 1);
$pdf->Cell(20, 7, 'Type', 1, 0, 'C', 1);
$pdf->Cell(35, 7, 'Plat Nomor', 1, 0, 'C', 1);
$pdf->Cell(30, 7, 'Berat', 1, 0, 'C', 1);
$pdf->Ln();
$pdf->Cell(25, 7, '' . $trans['port_of_origin'] . '', 1, 0, 'C', 1);
$pdf->Cell(25, 7, '' . $trans['port_of_destination'] . '', 1, 0, 'C', 1);
$pdf->Cell(35, 7, '' . $trans['iso_code'] . '', 1, 0, 'C', 1);
$pdf->Cell(20, 7, '' . $trans['eq_size'] . '', 1, 0, 'C', 1);
$pdf->Cell(20, 7, '' . $trans['eq_type'] . '', 1, 0, 'C', 1);
$pdf->Cell(35, 7, '' . $trans['in_truck_license_nbr'] . '', 1, 0, 'C', 1);
$pdf->Cell(30, 7, '' . $trans['tare_weight'] . '', 1, 0, 'C', 1);
$pdf->Ln();
//asignment
$det="";
if (!empty($equip))
{
    foreach ($equip as $v)
    {
			$filenaname="";
                            if ($v['bent_flag']=="Y")
                                $filenaname.=" bent,";
                            if ($v['Dentet_flag']=="Y")
                                $filenaname.=" dentet,";
                            if ($v['Leaking_flag']=="Y")
                                $filenaname.=" leaking,";
                            if ($v['PushIn_flag']=="Y")
                                $filenaname.=" pushin,";
                            if ($v['PushOut_flag']=="Y")
                                $filenaname.=" pushout,";
                            if ($v['Broke_flag']=="Y")
                                $filenaname.=" broke,";
                            if ($v['Hole_flag']=="Y")
                                $filenaname.=" hole,";
                            if ($v['Missing_flag']=="Y")
                                $filenaname.=" missing,";
                            if ($v['Cut_flag']=="Y")
                                $filenaname.=" cut,";
                            if ($v['Loose_flag']=="Y")
                                $filenaname.=" loose,";
                            if ($v['Tom_flag']=="Y")
                                $filenaname.=" tom,";
                            if ($v['Rusty_flag']=="Y")
                                $filenaname.=" rusty,";
                            $det=$v['location'] . "-" . $v['component']. "(".$filenaname."),";
	}
}else{
$det="No Damage";
}
$pdf->SetFillColor(255, 0, 0);
$pdf->SetTextColor(255);
$pdf->SetDrawColor(128, 0, 0);
$pdf->SetFont('Arial', '', 14);
$pdf->Cell(190, 7, 'Assignment', 1, 0, 'C', 1);
$pdf->Ln();
$pdf->SetX(10);
$pdf->SetFillColor(224, 235, 255);
$pdf->SetTextColor(0);
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(100, 22, $det, 1, 0, 'C', 1);
$pdf->Cell(45, 22, 'Reciever', 1, 0, 'C', 1);
$pdf->Cell(45, 22, 'Printed by : PT. Sucofindo', 1, 0, 'C', 1);

$pdf->Ln();

/* Footer */
$pdf->SetY(-35);
$pdf->Line(10, $pdf->GetY(), 275, $pdf->GetY());
$pdf->SetFont('Arial', 'I', 9);
$pdf->Cell(0, 10, 'Dicetak secara sistem oleh ' . $trans['depo_id'] . '', 0, 0, 'C');
$pdf->Cell(-15, 10, 'Halaman ' . $pdf->PageNo() . '', 0, 0, 'R');

//Halaman 2
$pdf->Header();
$pdf->AddPage('P');
/* Header */
$pdf->Cell(80);
$pdf->Cell(20, 0, 'EQUIPMENT INTERCHANGE RECEIPT (EIR)', 0, 0, 'C');
$pdf->Ln(2);
$pdf->SetFont('Arial', 'B', 15);
$pdf->Cell(80);
$pdf->Cell(20, 10, 'Incoming Container', 0, 0, 'C');
$pdf->Ln();
$pdf->Line(10, 25, 200, 25);
/* content */
$pdf->Ln();
$pdf->Cell(20, 10, 'Damage Details', 0, 0, 'L');
$pdf->SetFillColor(255, 0, 0);
$pdf->SetTextColor(255);
$pdf->SetDrawColor(128, 0, 0);
$pdf->SetFont('Arial', "B", 15);
$pdf->Ln();
//judul table
$pdf->Cell(35, 7, 'Component', 1, 0, 'C', 1);
$pdf->Cell(35, 7, 'Location', 1, 0, 'C', 1);
$pdf->Cell(10, 7, 'BE', 1, 0, 'C', 1);
$pdf->Cell(10, 7, 'DE', 1, 0, 'C', 1);
$pdf->Cell(10, 7, 'LE', 1, 0, 'C', 1);
$pdf->Cell(10, 7, 'PI', 1, 0, 'C', 1);
$pdf->Cell(10, 7, 'BR', 1, 0, 'C', 1);
$pdf->Cell(10, 7, 'HO', 1, 0, 'C', 1);
$pdf->Cell(10, 7, 'MI', 1, 0, 'C', 1);
$pdf->Cell(10, 7, 'PO', 1, 0, 'C', 1);
$pdf->Cell(10, 7, 'CU', 1, 0, 'C', 1);
$pdf->Cell(10, 7, 'LO', 1, 0, 'C', 1);
$pdf->Cell(10, 7, 'TO', 1, 0, 'C', 1);
$pdf->Cell(10, 7, 'RU', 1, 0, 'C', 1);
$pdf->Ln();
$pdf->SetX(10);
$pdf->SetFillColor(224, 235, 255);
$pdf->SetTextColor(0);
//isi tabel
$pdf->SetX(10);
$pdf->SetFillColor(224, 235, 255);
$pdf->SetTextColor(0);
$pdf->SetFont('Arial', '', 14);
if (!empty($equip))
{
    foreach ($equip as $v)
    {
        $pdf->Cell(35, 7, '' . $v['location'] . '', 1, 0, 'C', 1);
        $pdf->Cell(35, 7, '' . $v['component'] . '', 1, 0, 'C', 1);
        $pdf->Cell(10, 7, '' . $v['bent_flag'] == "Y" ? "Y" : "" . '', 1, 0, 'C', 1);
        $pdf->Cell(10, 7, '' . $v['Dentet_flag'] == "Y" ? "Y" : "" . '', 1, 0, 'C', 1);
        $pdf->Cell(10, 7, '' . $v['Leaking_flag'] == "Y" ? "Y" : "" . '', 1, 0, 'C', 1);
        $pdf->Cell(10, 7, '' . $v['PushIn_flag'] == "Y" ? "Y" : "" . '', 1, 0, 'C', 1);
        $pdf->Cell(10, 7, '' . $v['Broke_flag'] == "Y" ? "Y" : "" . '', 1, 0, 'C', 1);
        $pdf->Cell(10, 7, '' . $v['Hole_flag'] == "Y" ? "Y" : "" . '', 1, 0, 'C', 1);
        $pdf->Cell(10, 7, '' . $v['Missing_flag'] == "Y" ? "Y" : "" . '', 1, 0, 'C', 1);
        $pdf->Cell(10, 7, '' . $v['PushOut_flag'] == "Y" ? "Y" : "" . '', 1, 0, 'C', 1);
        $pdf->Cell(10, 7, '' . $v['Cut_flag'] == "Y" ? "Y" : "" . '', 1, 0, 'C', 1);
        $pdf->Cell(10, 7, '' . $v['Loose_flag'] == "Y" ? "Y" : "" . '', 1, 0, 'C', 1);
        $pdf->Cell(10, 7, '' . $v['Tom_flag'] == "Y" ? "Y" : "" . '', 1, 0, 'C', 1);
        $pdf->Cell(10, 7, '' . $v['Rusty_flag'] == "Y" ? "Y" : "" . '', 1, 0, 'C', 1);
        $pdf->Ln();
    }
    for ($i = 1; $i <= ceil((count($equip) / 3)); $i++)
    {
        $pdf->Header();
        $pdf->AddPage('P');
//Foto
        $pdf->SetFont('Arial', 'B', 15);
        $pdf->Cell(20, 10, 'Damage Foto', 0, 0, 'L');
        $pdf->SetFillColor(255, 0, 0);
        $pdf->SetTextColor(255);
        $pdf->SetDrawColor(128, 0, 0);
        $pdf->SetFont('Arial', "B", 15);
        $pdf->Ln();

        $pdf->Cell(190, 7, 'Foto', 1, 0, 'C', 1);
        $pdf->Ln();

        $pdf->SetX(10);
        $pdf->SetFillColor(224, 235, 255);
        $pdf->SetTextColor(0);
        $pdf->SetFont('Arial', '', 14);
        $pdf->Ln(5);
        foreach ($equip as $v)
        {
            $damage = "";
            if ($v['bent_flag'] == "Y")
                $damage.="Bent,";
            if ($v['Dentet_flag'] == "Y")
                $damage.="Bent,";
            if ($v['Leaking_flag'] == "Y")
                $damage.="Leaking,";
            if ($v['PushIn_flag'] == "Y")
                $damage.="Pushin,";
            if ($v['Broke_flag'] == "Y")
                $damage.="Broke,";
            if ($v['Hole_flag'] == "Y")
                $damage.="Hole,";
            if ($v['Missing_flag'] == "Y")
                $damage.="Missing,";
            if ($v['PushOut_flag'] == "Y")
                $damage.="Pushout,";
            if ($v['Cut_flag'] == "Y")
                $damage.="Cut,";
            if ($v['Loose_flag'] == "Y")
                $damage.="Loose,";
            if ($v['Tom_flag'] == "Y")
                $damage.="Tom,";
            if ($v['Rusty_flag'] == "Y")
                $damage.="Rusty";

            $pdf->Cell(80);
            $pdf->Image(BASE_URL . 'android/files/images/damage/' . $v['filename'] . '', 67, NULL, 80, 60);
            $pdf->Ln();
            $pdf->SetFillColor(255, 255, 255);
            $pdf->Cell(190, 8, '' . $damage . '', 0, 0, 'C', 1);
            $pdf->Ln();
            $pdf->Ln();
        }
    }
}else
{
    $pdf->Cell(190, 7, 'NO DAMAGE', 1, 0, 'C', 1);
}

//Halaman 3
$pdf->Header();
$pdf->AddPage('P');
/* Header */
$pdf->Cell(80);
$pdf->Cell(20, 0, 'EQUIPMENT INTERCHANGE RECEIPT (EIR)', 0, 0, 'C');
$pdf->Ln(2);
$pdf->SetFont('Arial', 'B', 15);
$pdf->Cell(80);
$pdf->Cell(20, 10, 'Incoming Container', 0, 0, 'C');
$pdf->Ln(5);
$pdf->Line(10, 25, 200, 25);
/* content */
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 15);
$pdf->Cell(20, 10, 'Full Foto', 0, 0, 'L');
$pdf->SetFillColor(255, 0, 0);
$pdf->SetTextColor(255);
$pdf->SetDrawColor(128, 0, 0);
$pdf->SetFont('Arial', "B", 15);
$pdf->Ln();

$pdf->Cell(190, 7, 'Foto', 1, 0, 'C', 1);
$pdf->Ln();

$pdf->SetX(10);
$pdf->SetFillColor(224, 235, 255);
$pdf->SetTextColor(0);
$pdf->SetFont('Arial', '', 14);
$pdf->Ln(5);
$hitung=0;
foreach ($full as $v)
{
    if ($v['rightside'] != NULL)
        $hitung++;
    if ($v['leftside'] != NULL)
        $hitung++;
    if ($v['bottomside'] != NULL)
        $hitung++;
    if ($v['topside'] != NULL)
        $hitung++;
    if ($v['rearside'] != NULL)
        $hitung++;
    if ($v['frontside'] != NULL)
        $hitung++;
    if ($v['inside'] != NULL)
        $hitung++;
}

for($a=1;$a<=ceil(count($hitung) / 3);$a++){
    foreach ($full as $v)
    {
    // $pdf->Header();
    // $pdf->AddPage('P');
        $total = 0;
        if ($v['rightside'] != NULL)
        {
            $pdf->Cell(80);
            $pdf->Image(BASE_URL . 'android/files/images/full/' . $v['rightside'] . '', 67, NULL, 80, 60);
            $pdf->Ln();
            $pdf->SetFillColor(255, 255, 255);
            $pdf->Cell(190, 8, 'Right Side', 0, 0, 'C', 1);
            $pdf->Ln();
            $total++;
        }
        $pdf->Ln();
        if ($v['leftside'] != NULL)
        {
            $pdf->Cell(80);
            $pdf->Image(BASE_URL . 'android/files/images/full/' . $v['leftside'] . '', 67, NULL, 80, 60);
            $pdf->Ln();
            $pdf->SetFillColor(255, 255, 255);
            $pdf->Cell(190, 8, 'Left Side', 0, 0, 'C', 1);
            $pdf->Ln();
            $total++;
        }
        $pdf->Ln();
        if ($v['rearside'] != NULL)
        {
            $pdf->Cell(80);
            $pdf->Image(BASE_URL . 'android/files/images/full/' . $v['rearside'] . '', 67, NULL, 80, 60);
            $pdf->Ln();
            $pdf->SetFillColor(255, 255, 255);
            $pdf->Cell(190, 8, 'Rear Side', 0, 0, 'C', 1);
            $pdf->Ln();
            $total++;
        }
        $pdf->Ln();
        if ($v['frontside'] != NULL)
        {
            $pdf->Cell(80);
            $pdf->Image(BASE_URL . 'android/files/images/full/' . $v['frontside'] . '', 67, NULL, 80, 60);
            $pdf->Ln();
            $pdf->SetFillColor(255, 255, 255);
            $pdf->Cell(190, 8, 'Front Side', 0, 0, 'C', 1);
            $pdf->Ln();
            $total++;
        }
        $pdf->Ln();
        if ($v['topside'] != NULL)
        {
            $pdf->Cell(80);
            $pdf->Image(BASE_URL . 'android/files/images/full/' . $v['topside'] . '', 67, NULL, 80, 60);
            $pdf->Ln();
            $pdf->SetFillColor(255, 255, 255);
            $pdf->Cell(190, 8, 'Top Side', 0, 0, 'C', 1);
            $pdf->Ln();
            $total++;
        }
        $pdf->Ln();
        if ($v['bottomside'] != NULL)
        {
            $pdf->Cell(80);
            $pdf->Image(BASE_URL . 'android/files/images/full/' . $v['bottomside'] . '', 67, NULL, 80, 60);
            $pdf->Ln();
            $pdf->SetFillColor(255, 255, 255);
            $pdf->Cell(190, 8, 'Bottom Side', 0, 0, 'C', 1);
            $pdf->Ln();
            $total++;
        }
        $pdf->Ln();
        if ($v['inside'] != NULL)
        {
            $pdf->Cell(80);
            $pdf->Image(BASE_URL . 'android/files/images/full/' . $v['inside'] . '', 67, NULL, 80, 60);
            $pdf->Ln();
            $pdf->SetFillColor(255, 255, 255);
            $pdf->Cell(190, 8, 'Inside', 0, 0, 'C', 1);
            $pdf->Ln();
            $total++;
        }
       /* if ($total == 3)
        {
            $pdf->Cell(80);
            $pdf->Cell(20, 0, 'EQUIPMENT INTERCHANGE RECEIPT (EIR)', 0, 0, 'C');
            $pdf->Ln(2);
            $pdf->SetFont('Arial', 'B', 15);
            $pdf->Cell(80);
            $pdf->Cell(20, 10, 'Incoming Container', 0, 0, 'C');
            $pdf->Ln(5);
            $pdf->Line(10, 25, 200, 25);
            $pdf->Ln(10);
            $pdf->SetFont('Arial', 'B', 15);
            $pdf->Cell(20, 10, 'Full Foto', 0, 0, 'L');
            $pdf->SetFillColor(255, 0, 0);
            $pdf->SetTextColor(255);
            $pdf->SetDrawColor(128, 0, 0);
            $pdf->SetFont('Arial', "B", 15);
            $pdf->Ln();

            $pdf->Header();
            $pdf->AddPage('P');
        }*/
    }
}
$pdf->Output();
?>
