<html>

    <head>

        <title>EIR</title>

        <style type="text/css">

            table{

                width: 960px;		

            }

            table, td, th{

                border-collapse:collapse;

                border:1px solid #000;

                font-size: 12px

            }

            .head{

                background: #DCEBF9;

                text-align: center;

            }

            .wrapper{

                width:960px;

                margin:0 auto;

            }

            .center{

                text-align: center;

            }

            .col2{

                width:50%;

            }

            .col3{

                width:33.333333%;

            }	

            .col4{

                width:25%;

            }

            .box td{

                border:none;

            }

            .check{

                width: 20px;

                height:20px;

                border: 1px solid #000;

            }

            .left{

                float:left;

                margin-right: 5px

            }

            .text-right{

                text-align: right;

            }

            .row{

                display:block;

                padding: 5px;

                width: 150px;

                margin-left:100px;

            }



        </style>

    </head>

    <body>

        <div class="wrapper">

            <table class="box">

                <tr>

                    <td>

                        <img src="<?= BASE_ASSET . 'img/logo/az_subtle.png'; ?>">				

                    </td>

                    <td class="center">

                        <span style="text-decoration:underline;">EQUIPMENT INTERCHANGE REPORT</span>

                    </td>

                    <td class="text-right">

                        <img src="<?= BASE_ASSET . 'img/logo/scilogo.jpg'; ?>">				

                    </td>

                </tr>

                <tr>

                    <td>

                        PT GEMA NAWAGRAHASEJATI (GNS) <br>

                        DEPO II : <?= $trans['depo_id']; ?> <br>

                        member of ASDEKI

                    </td>

                    <td class="center">

                        <div class="row">

                            <?php

                            if ($transaction['trx_type_id'] == 'EXIMP')

                            {

                                ?>

                                <div class="check left">X</div> <div class="left">Gate in</div> 

                                <?php

                            }

                            else

                            {

                                ?>

                                <div class="check left"></div> <div class="left">Gate in</div> 

                                <?php

                            }

                            ?>

                            <?php

                            if ($transaction['trx_type_id'] == 'TOEXP')

                            {

                                ?>

                                <div class="check left">X</div> <div class="left">Gate out</div> 

                                <?php

                            }

                            else

                            {

                                ?>

                                <div class="check left"></div> <div class="left">Gate out</div> 

                                <?php

                            }

                            ?>

                        </div>

                    </td>	

                    <td class="text-right">

                        Jln. Raya Pasar Minggu Kav.34 <br>

                        Jakarta Selatan 12780 <br>

                        www.sucofindo.co.id

                    </td>

                </tr>

            </table>

            <table>

                <tr>

                    <td>EIR Series No. : <?= $eir_no . " / SCI-ASDEKI / " . $bulan . " / " . $tahun; ?></td>

                    <td>Shipping Agent : <?= $trans['shipping_agent_name']; ?></td>

                </tr>

                <tr>

                    <td>Ex Consignee : <?= $trans['consignee']; ?></td>

                    <td>Trucking Co. : <?= $transaction['trucking_company_name']; ?></td>

                </tr>

                <tr>

                    <td>Ex Vessel/Voy No. : <?= $transaction['vessel_name'] . " / " . $transaction['vessel_voyage_id']; ?></td>

                    <td>Vehicle No. : <?= $transaction['truck_license_nbr']; ?></td>

                </tr>

                <tr>

                    <td>Printed Date and Time : <?= $tanggal . "-" . $bulan . "-" . $tahun . " / " . $jam . ":" . $menit; ?></td>

                    <td>Return Date : <?= $tanggal . "-" . $bulan . "-" . $tahun; ?></td>

                </tr>

            </table>

            <table>

                <tr class="head">

                    <td>CONTAINER PREFIX &amp; NUMBER</td>

                    <td>SIZE</td>

                    <td>TYPE</td>

                    <td>CONDITION</td>

                    <td>DATE MANUFACTURE</td>

                </tr>

                <tr class="center">

                    <td><?= $eq_nbr; ?></td>

                    <td><?= $trans['eq_size']; ?></td>

                    <td><?= $trans['eq_type']; ?></td>

                    <td></td>

                    <td></td>

                </tr>

            </table>

            <table class="head">

                <tr>

                    <td>DAMAGE TYPE CODES (ISO CODES - CEDEX)</td>

                </tr>

            </table>

            <table>

                <tr>			

                    <td>BN	: Burned		</td>

                    <td>CU	: Cut		</td>

                    <td>DB	: Debris		</td>

                    <td>NL	: Nails		</td>

                    <td>RO	: Rotted		</td>

                    <td>CL	: Compression Line			</td>

                    <td>SR	: Shrunk			</td>

                    <td>NO	: Not as Req Owner		</td>

                </tr>

                <tr>

                    <td>BR	: Broken</td>

                    <td>CO	: Rusty</td>

                    <td>DT	: Dented</td>

                    <td>OR	: Odor</td>

                    <td>RP	: Replaced</td>

                    <td>CT	: Contamination	</td>

                    <td>WA	: Warped	</td>

                    <td>NV	: Not as Req User</td>

                </tr>

                <tr>

                    <td>BT	: Bent		</td>

                    <td>LO	: Loose		</td>

                    <td>DY	: Dirty		</td>

                    <td>OS	: Oil Stains		</td>

                    <td>RT	: Rotten		</td>

                    <td>DL	: Delamination			</td>

                    <td>WM	: Wrong Material			</td>

                    <td>OD	: Out Of Date		</td>

                </tr>

                <tr>

                    <td>BW	: Bowed		</td>

                    <td>ML	: Marking		</td>

                    <td>FZ	: Frozen		</td>

                    <td>PH	: Pin Holes		</td>

                    <td>SD	: Stretched		</td>

                    <td>IR	: Improper Repair			</td>

                    <td>WT	: Wear & Tear			</td>

                    <td>OL	: Oil Satured		</td>

                </tr>

                <tr>

                    <td>CK	: Cracked		</td>

                    <td>MS	: Missing		</td>

                    <td>HO	: Holed		</td>

                    <td>RM	: Remove		</td>

                    <td>SN	: Section		</td>

                    <td>NI	: Not w/ ISO Dim			</td>

                    <td>ZZ	: Not Applicable			</td>

                    <td>PF	: Paint Failure		</td>

                </tr>

            </table>

            <table class="head">

                <tr>

                    <td>FIGURES</td>

                </tr>

            </table>

            <table class="center">

                <tr>

                    <td>LEFT - FRONT - BOTTOM PANEL</td>

                    <td colspan="2">RIGHT - DOOR - FRONT PANEL</td>

                </tr>

                <tr>

                    <td>

                        <img src="<?= BASE_ASSET . 'img/logo/leftside.png'; ?>">

                    </td>

                    <td colspan="2">

                        <img src="<?= BASE_ASSET . 'img/logo/rightside.png'; ?>">

                    </td>

                </tr>

                <tr>

                    <td>DOOR PANEL</td>

                    <td colspan="2">INSIDE PANEL</td>

                </tr>

                <tr>

                    <td>

                        <img src="<?= BASE_ASSET . 'img/logo/doorside.png'; ?>">

                    </td>

                    <td colspan="2">

                        <img src="<?= BASE_ASSET . 'img/logo/inside.png'; ?>">

                    </td>

                </tr>

                <tr class="head">

                    <td>DETAILS :</td>

                    <td>For Receiving Delivery Carrier,</td>

                    <td>Printed &amp; Checked By</td>

                </tr>

                <tr>

                    <td>

                        <?php

                        $index = 1;

                        $y = 245;

                        foreach ($equip as $oi => $v)

                        {

                            $damage = "";

                            if ($v['bent_flag'] == "Y")

                                $damage.=" -BT";

                            if ($v['Dentet_flag'] == "Y")

                                $damage.="-DT";

                            if ($v['Leaking_flag'] == "Y")

                                $damage.="-LK";

                            if ($v['PushIn_flag'] == "Y")

                                $damage.="-PI";

                            if ($v['Broke_flag'] == "Y")

                                $damage.="-BR";

                            if ($v['Hole_flag'] == "Y")

                                $damage.="-HO";

                            if ($v['Missing_flag'] == "Y")

                                $damage.="-MS";

                            if ($v['PushOut_flag'] == "Y")

                                $damage.="-PO";

                            if ($v['Cut_flag'] == "Y")

                                $damage.="-CU";

                            if ($v['Loose_flag'] == "Y")

                                $damage.="-LO";

                            if ($v['Tom_flag'] == "Y")

                                $damage.="-TM";

                            if ($v['Rusty_flag'] == "Y")

                                $damage.="-CO";

                            $outs = strtoupper(substr($v['location'], 0, 1)) . "_" . $v['component'] . "" . $damage;

                            echo $outs . "<br>";

                            $y+=5;

                            $index++;

                        }

                        ?>

                    </td>

                    <td></td>

                    <td>
                        PT. SUCOFINDO (Persero)<br>
                        <br><br><br><br><br><br><br>
                    </td>

                </tr>

            </table>

            <table class="head">

                <tr>

                    <td>5 SIDE CONTAINER PANEL FIGURES:</td>

                </tr>

            </table>

            <table class="center">

                <tr>

                    <td>RIGHT SIDE PANEL</td>

                    <td>LEFT SIDE PANEL</td>

                    <td>TOP SIDE PANEL</td>

                </tr>

                <tr>

                    <td>

                        <?php

                        if ($full['rightside'] != "")

                        {

                            ?>

                            <img src="<?= BASE_URL . 'android/files/images/full/' . $full['rightside']; ?>" width="250px" height="150px">

                            <?php

                        }

                        ?>

                    </td>

                    <td>

                        <?php

                        if ($full['leftside'] != "")

                        {

                            ?>

                            <img src="<?= BASE_URL . 'android/files/images/full/' . $full['leftside']; ?>"width="250px" height="150px">

                            <?php

                        }

                        ?>

                    </td>

                    <td>

                        <?php

                        if ($full['topside'] != "")

                        {

                            ?>

                            <img src="<?= BASE_URL . 'android/files/images/full/' . $full['topside']; ?>"width="250px" height="150px">

                            <?php

                        }

                        ?>

                    </td>

                </tr>

            </table>

            <table class="center">

                <tr>

                    <td>DOOR PANEL</td>

                    <td>INSIDE PANEL</td>

                </tr>

                <tr>

                    <td>

                        <?php

                        if ($full['rearside'] != "")

                        {

                            ?>

                            <img src="<?= BASE_URL . 'android/files/images/full/' . $full['rearside']; ?>"width="250px" height="150px">

                            <?php

                        }

                        ?>

                    </td>

                    <td>

                        <?php

                        if ($full['inside'] != "")

                        {

                            ?>

                            <img src="<?= BASE_URL . 'android/files/images/full/' . $full['inside']; ?>"width="250px" height="150px">

                            <?php

                        }

                        ?>

                    </td>

                </tr>

            </table>

            <table class="head">

                <tr>

                    <td>DAMAGE FIGURES</td>

                </tr>

            </table>

            <table>

                <tr>

                    <?php

                    foreach ($equip as $oi => $v)

                    {

                        ?>

                        <td>Details: <?= strtoupper(substr($v['location'], 0, 1)) . "_" . $v['component'] . "" . $damage; ?></td>

                        <?php

                        $index++;

                    }

                    ?>

                </tr>

                <tr class="center">

                    <?php

                    foreach ($equip as $oi => $v)

                    {

                        ?>

                        <td>

                            <img src="<?= BASE_URL . 'android/files/images/damage/' . $v['filename']; ?>"width="250px" height="150px">

                        </td>

                        <?php

                        $index++;

                    }

                    ?>

                </tr>

            </table>

        </div>

    </body>

</html>