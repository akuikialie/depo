<?php

$eq_nbr = $eq_nbr;
define('FPDF_FONTPATH', 'fpdf17/font/');
require('fpdf17/fpdf.php');

class PDF extends FPDF {

    var $tablewidths;
    var $headerset;
    var $footerset;

    //Page header
    function Header()
    {
        //Logo
        //Arial bold 15
        $this->SetFont('Arial', 'B', 15);
        //pindah ke posisi ke tengah untuk membuat judul
        $this->Cell(80);
        //judul
        $this->Cell(30, 10, 'EQUIPMENT INTERCHANGE RECEIPT (EIR)', 0, 0, 'C');
        //pindah baris
        $this->Ln(20);
        //buat garis horisontal
        $this->Line(10, 25, 200, 25);
    }

    //Page Content
    function Content()
    {
        $this->SetFont('Times', '', 12);
        for ($i = 1; $i <= 40; $i++)
            $this->Cell(0, 10, 'line report ' . $i, 0, 1);
    }

    //Page footer
    function Footer()
    {
        //atur posisi 1.5 cm dari bawah
        $this->SetY(-15);
        //buat garis horizontal
        $this->Line(10, $this->GetY(), 200, $this->GetY());
        //Arial italic 9
        $this->SetFont('Arial', 'I', 9);
        //nomor halaman
        $this->Cell(-15, 10, 'Halaman ' . $this->PageNo() . ' dari {nb}', 0, 0, 'R');
    }

}

$pdf = new FPDF();
//Halaman 1

$pdf->_beginpage('P', 'legal');
$pdf->SetY(5);
$pdf->Image(BASE_ASSET . 'img/logo/az_subtle.png', 14, 8);
$pdf->Image(BASE_ASSET . 'img/logo/scilogo.jpg', 175, 6);

$pdf->Cell(16);
$pdf->SetFont('Arial', 'B', 12);
$pdf->Line(77, 15, 155, 15);
$pdf->Cell(0, 15, 'EQUIPMENT INTERCHANGE REPORT', 0, 0, 'C');
$pdf->SetX(10);
$pdf->Cell(200, 33, '', 1, 0, 'C');
$pdf->SetX(10);
$pdf->SetFont('Arial', '', 9);
$pdf->Ln(18);
$pdf->Cell(70, 5, 'PT. GEMA NAWAGRAHA SEJATI (GNS)', 0, 0, 'L');
$pdf->SetFont('Arial', '', 15);
$pdf->SetTextColor(0, 0, 255);
if ($transaction['trx_type_id'] == 'EXIMP')
{
    $pdf->Cell(10, 10, 'X', 1, 0, 'C');
}
else
{
    $pdf->Cell(10, 10, '', 1, 0, 'C');
}
$pdf->SetFont('Arial', '', 9);
$pdf->SetTextColor(0, 0, 0);
$pdf->Cell(20, 10, 'Gate In', 1, 0, 'C');
$pdf->SetFont('Arial', '', 15);
$pdf->SetTextColor(0, 0, 255);
if ($transaction['trx_type_id'] == 'TOEXP')
{
    $pdf->Cell(10, 10, 'X', 1, 0, 'C');
}
else
{
    $pdf->Cell(10, 10, '', 1, 0, 'C');
}
$pdf->SetFont('Arial', '', 9);
$pdf->SetTextColor(0, 0, 0);
$pdf->Cell(20, 10, 'Gate Out', 1, 0, 'C');
//$pdf->Cell(40, 10, '', 1, 0, 'L');
$pdf->Cell(70, 5, 'Jln. Raya Pasar Minggu Kav.34', 0, 0, 'R');
$pdf->Ln();
$pdf->Cell(20, 5, 'Depo', 0, 0, 'L');
$pdf->Cell(5, 5, ':', 0, 0, 'L');
$pdf->Cell(45, 5, $trans['depo_id'], 0, 0, 'L');
$pdf->Cell(60, 5, '', 0, 0, 'L');
$pdf->Cell(70, 5, 'Jakarta Selatan 12780', 0, 0, 'R');
$pdf->Ln();
$pdf->Cell(70, 5, 'Member of ASDEKI', 0, 0, 'L');
$pdf->Cell(60, 5, '', 0, 0, 'L');
$pdf->Cell(70, 5, 'www.sucofindo.co.id', 0, 0, 'R');
$pdf->Ln();
$pdf->Cell(40, 5, 'EIR Series No.', 0, 0, 'L');
$pdf->Cell(5, 5, ':', 0, 0, 'C');
$pdf->Cell(55, 5, $eir_no . " / SCI-ASDEKI / " . $bulan . " / " . $tahun, 0, 0, 'L');
$pdf->Cell(40, 5, 'Shipping Agent', 0, 0, 'L');
$pdf->Cell(5, 5, ':', 0, 0, 'C');
$pdf->Cell(55, 5, $trans['shipping_agent_name'], 0, 0, 'L');
$pdf->SetX(10);
$pdf->Cell(100, 5, '', 1, 0, 'C');
$pdf->Cell(100, 5, '', 1, 0, 'C');
$pdf->Ln();
$pdf->Cell(40, 5, 'Ex Consignee', 0, 0, 'L');
$pdf->Cell(5, 5, ':', 0, 0, 'C');
$pdf->Cell(55, 5, $trans['consignee'], 0, 0, 'L');
$pdf->Cell(40, 5, 'Trucking Co.', 0, 0, 'L');
$pdf->Cell(5, 5, ':', 0, 0, 'C');
$pdf->Cell(55, 5, $transaction['trucking_company_name'], 0, 0, 'L');
$pdf->SetX(10);
$pdf->Cell(100, 5, '', 1, 0, 'C');
$pdf->Cell(100, 5, '', 1, 0, 'C');
$pdf->Ln();
$pdf->Cell(40, 5, 'Ex Vessel/Voy No.', 0, 0, 'L');
$pdf->Cell(5, 5, ':', 0, 0, 'C');
$pdf->Cell(55, 5, $transaction['vessel_name'] . " / " . $transaction['vessel_voyage_id'], 0, 0, 'L');
$pdf->Cell(40, 5, 'Vehicle No.', 0, 0, 'L');
$pdf->Cell(5, 5, ':', 0, 0, 'C');
$pdf->Cell(55, 5, $transaction['truck_license_nbr'], 0, 0, 'L');
$pdf->SetX(10);
$pdf->Cell(100, 5, '', 1, 0, 'C');
$pdf->Cell(100, 5, '', 1, 0, 'C');
$pdf->Ln();
$pdf->Cell(40, 5, 'Printed Date & Time', 0, 0, 'L');
$pdf->Cell(5, 5, ':', 0, 0, 'C');
$pdf->Cell(55, 5, $tanggal . "-" . $bulan . "-" . $tahun . " / " . $jam . ":" . $menit, 0, 0, 'L');
$pdf->Cell(40, 5, 'Return Date', 0, 0, 'L');
$pdf->Cell(5, 5, ':', 0, 0, 'C');
$pdf->Cell(55, 5, $tanggal . "-" . $bulan . "-" . $tahun, 0, 0, 'L');
$pdf->SetX(10);
$pdf->Cell(100, 5, '', 1, 0, 'C');
$pdf->Cell(100, 5, '', 1, 0, 'C');
$pdf->Ln();
$pdf->SetX(10);
$pdf->SetFont('Arial', "B", 12);
$pdf->Cell(70, 7, 'CONTAINER PREFIX & NUMBER', 1, 0, 'C');
$pdf->Cell(20, 7, 'Size', 1, 0, 'C');
$pdf->Cell(25, 7, 'Type', 1, 0, 'C');
$pdf->Cell(35, 7, 'CONDITION', 1, 0, 'C');
$pdf->Cell(50, 7, 'DATE MANUFACTURE', 1, 0, 'C');
$pdf->Ln();
$pdf->SetX(10);
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(70, 5, $eq_nbr, 1, 0, 'C');
$pdf->Cell(20, 5, $trans['eq_size'], 1, 0, 'C');
$pdf->Cell(25, 5, $trans['eq_type'], 1, 0, 'C');
$pdf->Cell(35, 5, '', 1, 0, 'C');
$pdf->Cell(50, 5, '', 1, 0, 'C');
$pdf->Ln();
$pdf->SetX(10);
$pdf->SetFont('Arial', "B", 12);
$pdf->Cell(200, 7, 'DAMAGE TYPE CODES (ISO CODES - CEDEX)', 1, 0, 'C');
$pdf->Ln();
$pdf->SetX(10);
$pdf->SetFont('times', '', 6);
$no = 1;
foreach ($damages as $a => $h)
{
    $pdf->Cell(5, 5, $h['code'], 0, 0, 'C');
    $pdf->Cell(3, 5, ':', 0, 0, 'C');
    $pdf->Cell(17, 5, $h['description'], 0, 0, 'C');
    if ($no == 8 || $no == 16 || $no == 24 || $no == 32 || $no == 40)
    {
        $pdf->SetX(10);
        $pdf->Cell(25, 5, '', 1, 0, 'C');
        $pdf->Cell(25, 5, '', 1, 0, 'C');
        $pdf->Cell(25, 5, '', 1, 0, 'C');
        $pdf->Cell(25, 5, '', 1, 0, 'C');
        $pdf->Cell(25, 5, '', 1, 0, 'C');
        $pdf->Cell(25, 5, '', 1, 0, 'C');
        $pdf->Cell(25, 5, '', 1, 0, 'C');
        $pdf->Cell(25, 5, '', 1, 0, 'C');
        $pdf->SetX(10);
        $pdf->Ln();
    }
    $no++;
}
$pdf->SetX(10);
$pdf->SetFont('Arial', "B", 12);
$pdf->Cell(200, 7, 'FIGURES:', 1, 0, 'C');
$pdf->Ln();
$pdf->SetX(10);
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(100, 5, 'LEFT - FRONT - BOTTOM PANEL', 1, 0, 'C');
$pdf->Cell(100, 5, 'RIGHT - DOOR - FRONT PANEL', 1, 0, 'C');
$pdf->Ln();
$pdf->SetX(10);
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(100, 60, 'A', 1, 0, 'C');
$pdf->Cell(100, 60, 'B', 1, 0, 'C');
$pdf->Ln();
$pdf->SetX(10);
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(100, 60, 'C', 1, 0, 'C');
$pdf->Cell(100, 60, 'D', 1, 0, 'C');
$pdf->Ln();

$pdf->SetX(10);
$pdf->SetFont('Arial', "B", 12);
$pdf->Cell(100, 7, 'DETAILS:', 1, 0, 'L');
$pdf->SetFont('Arial', "B", 10);
$pdf->Cell(55, 7, 'For Receiving Delivery Carrier,', 1, 0, 'C');
$pdf->Cell(45, 7, 'Printed & Checked By,', 1, 0, 'C');
$pdf->Ln();
$pdf->SetX(10);
$pdf->SetFont('Arial', "", 8);
$index = 1;
$y = 245;
foreach ($equip as $oi => $v)
{
    $damage = "";
    if ($v['bent_flag'] == "Y")
        $damage.=" -BT";
    if ($v['Dentet_flag'] == "Y")
        $damage.="-DT";
    if ($v['Leaking_flag'] == "Y")
        $damage.="-LK";
    if ($v['PushIn_flag'] == "Y")
        $damage.="-PI";
    if ($v['Broke_flag'] == "Y")
        $damage.="-BR";
    if ($v['Hole_flag'] == "Y")
        $damage.="-HO";
    if ($v['Missing_flag'] == "Y")
        $damage.="-MS";
    if ($v['PushOut_flag'] == "Y")
        $damage.="-PO";
    if ($v['Cut_flag'] == "Y")
        $damage.="-CU";
    if ($v['Loose_flag'] == "Y")
        $damage.="-LO";
    if ($v['Tom_flag'] == "Y")
        $damage.="-TM";
    if ($v['Rusty_flag'] == "Y")
        $damage.="-CO";
    $pdf->Text(14, $y, strtoupper(substr($v['location'], 0, 1)) . "_" . $v['component'] . "" . $damage);
    $y+=5;
    $index++;
}
$pdf->Cell(100, 50, '', 1, 0, 'L');
$pdf->Cell(55, 50, '', 1, 0, 'C');
$pdf->SetFont('helvetica', "B", 8);
$pdf->Cell(45, 7, 'PT. SUCOFINDO (Persero)', 1, 0, 'C');
$pdf->Ln();
$pdf->Cell(155, 43, '', 0, 0, 'C');
$pdf->Cell(45, 43, '', 1, 0, 'C');


//image damage web
$pdf->SetY(5);
$pdf->Image(BASE_ASSET . 'img/logo/leftside.png', 17, 120);
$pdf->Image(BASE_ASSET . 'img/logo/rightside.png', 116, 120);
$pdf->Image(BASE_ASSET . 'img/logo/doorside.png', 17, 180);
$pdf->Image(BASE_ASSET . 'img/logo/inside.png', 116, 180);

//garis damage
$pdf->SetDrawColor(255, 0, 0);
$pdf->SetLineWidth(0.75);


foreach ($equip as $j => $h)
{
    //LEFT SIDE 
    if ($h['location'] == 'LEFTFRONTTOP' && $h['component'] == 'CORNER CASTINGS')
    {
        $pdf->SetXY(17, 121);
        $pdf->Cell(26, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'LEFTTOP' && $h['component'] == 'SIDE RAIL')
    {
        $pdf->SetXY(55, 121);
        $pdf->Cell(17, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'LEFTTOPREAR' && $h['component'] == 'CORNER CASTINGS')
    {
        $pdf->SetXY(79, 121);
        $pdf->Cell(23, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'TOPFRONT' && $h['component'] == 'RAIL')
    {
        $pdf->SetXY(17, 129);
        $pdf->Cell(16, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'LEFTFRONT' && $h['component'] == 'CORNER POSTS')
    {
        $pdf->SetXY(18, 137.5);
        $pdf->Cell(17, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'BOTTOMFRONT' && $h['component'] == 'RAIL')
    {
        $pdf->SetXY(18.5, 147);
        $pdf->Cell(12.5, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'LEFTBOTTOMFRONT' && $h['component'] == 'CORNER CASTINGS')
    {
        $pdf->SetXY(17, 157.5);
        $pdf->Cell(21, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'BOTTOM' && $h['component'] == 'PANEL')
    {
        $pdf->SetXY(28, 164.5);
        $pdf->Cell(17, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'LEFTBOTTOM' && $h['component'] == 'SIDE RAIL')
    {
        $pdf->SetXY(56, 160.5);
        $pdf->Cell(12, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'LEFT' && $h['component'] == 'PANEL')
    {
        $pdf->SetXY(84, 134.5);
        $pdf->Cell(14, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'LEFTREAR' && $h['component'] == 'CORNER POSTS')
    {
        $pdf->SetXY(83, 141);
        $pdf->Cell(19, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'LEFT' && $h['component'] == 'FORKLIFT CASTINGS')
    {
        $pdf->SetXY(83, 149);
        $pdf->Cell(18, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'LEFTBOTTOMREAR' && $h['component'] == 'CORNER CASTINGS')
    {
        $pdf->SetXY(81, 157);
        $pdf->Cell(21, 3.5, '', 1, 0, 'C');
    }
    //RIGHT SIDE
    if ($h['location'] == 'RIGHTTOP' && $h['component'] == 'SIDE RAIL')
    {
        $pdf->SetXY(152, 121);
        $pdf->Cell(21, 3.5, '', 1, 0, 'C');
    }

    if ($h['location'] == 'TOP' && $h['component'] == 'PANEL')
    {
        $pdf->SetXY(117, 126);
        $pdf->Cell(17, 3.5, '', 1, 0, 'C');
    }

    if ($h['location'] == 'REARTOPRIGHT' && $h['component'] == 'CORNER CASTINGS')
    {
        $pdf->SetXY(116, 132);
        $pdf->Cell(22, 3.5, '', 1, 0, 'C');
    }

    if ($h['location'] == 'TOPREAR' && $h['component'] == 'DOOR HEADER')
    {
        $pdf->SetXY(117, 137.5);
        $pdf->Cell(18, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'REARRIGHT' && $h['component'] == 'CORNER POSTS')
    {
        $pdf->SetXY(117, 149.5);
        $pdf->Cell(19, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'BOTTOMREAR' && $h['component'] == 'DOOR SIIL')
    {
        $pdf->SetXY(117, 158.5);
        $pdf->Cell(21, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'REARBOTTOMRIGHT' && $h['component'] == 'CORNER CASTINGS')
    {
        $pdf->SetXY(117, 164.5);
        $pdf->Cell(21, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'RIGHTBOTTOM' && $h['component'] == 'SIDE RAIL')
    {
        $pdf->SetXY(151.5, 160);
        $pdf->Cell(24, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'RIGHTBOTTOMFRONT' && $h['component'] == 'CORNER CASTINGS')
    {
        $pdf->SetXY(181, 158);
        $pdf->Cell(22, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'RIGHT' && $h['component'] == 'FORKLIFT POCKETS')
    {
        $pdf->SetXY(184, 145);
        $pdf->Cell(19, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'RIGHT' && $h['component'] == 'PANEL')
    {
        $pdf->SetXY(184, 141);
        $pdf->Cell(19, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'RIGHTFRONT' && $h['component'] == 'CORNER POSTS')
    {
        $pdf->SetXY(183.5, 133.5);
        $pdf->Cell(20, 3.5, '', 1, 0, 'C');
    }
    if ($h['location'] == 'RIGHTOPFRONT' && $h['component'] == 'CORNER CASTINGS')
    {
        $pdf->SetXY(182, 126.5);
        $pdf->Cell(22, 3.5, '', 1, 0, 'C');
    }

    //DOOR SIDE
    if ($h['location'] == 'DOOR' && $h['component'] == 'J BAR')
    {
        $pdf->SetXY(17.5, 192);
        $pdf->Cell(21, 5, '', 1, 0, 'C');
    }

    if ($h['location'] == 'DOOR' && $h['component'] == 'LEFT DOOR LEAF')
    {
        $pdf->SetXY(17.5, 202);
        $pdf->Cell(21, 5, '', 1, 0, 'C');
    }

    if ($h['location'] == 'DOOR' && $h['component'] == 'LOCKING BAR HANDLE')
    {
        $pdf->SetXY(18, 214.5);
        $pdf->Cell(21, 8, '', 1, 0, 'C');
    }

    if ($h['location'] == 'DOOR' && $h['component'] == 'DOOR CAM KEEPER')
    {
        $pdf->SetXY(82.5, 219);
        $pdf->Cell(19, 9, '', 1, 0, 'C');
    }

    if ($h['location'] == 'DOOR' && $h['component'] == 'DOOR HINGE')
    {
        $pdf->SetXY(82.5, 210);
        $pdf->Cell(19, 5, '', 1, 0, 'C');
    }

    if ($h['location'] == 'DOOR' && $h['component'] == 'RIGHT DOOR LEAF')
    {
        $pdf->SetXY(82.5, 203);
        $pdf->Cell(19, 5, '', 1, 0, 'C');
    }

    if ($h['location'] == 'DOOR' && $h['component'] == 'LOCKING BAR')
    {
        $pdf->SetXY(82.5, 190);
        $pdf->Cell(19, 5, '', 1, 0, 'C');
    }

    //INSIDE
    if ($h['location'] == 'INSIDE' && $h['component'] == 'ROOF PANEL INSIDE')
    {
        $pdf->SetXY(156, 181);
        $pdf->Cell(14, 9, '', 1, 0, 'C');
    }
    if ($h['location'] == 'INSIDE' && $h['component'] == 'FRONT PANEL INSIDE')
    {
        $pdf->SetXY(153, 193);
        $pdf->Cell(19, 4.5, '', 1, 0, 'C');
    }

    if ($h['location'] == 'INSIDE' && $h['component'] == 'LEFT DOOR INSIDE')
    {
        $pdf->SetXY(117.5, 198);
        $pdf->Cell(19, 5, '', 1, 0, 'C');
    }

    if ($h['location'] == 'INSIDE' && $h['component'] == 'LEFT PANEL INSIDE')
    {
        $pdf->SetXY(145, 205);
        $pdf->Cell(14, 8.5, '', 1, 0, 'C');
    }

    if ($h['location'] == 'INSIDE' && $h['component'] == 'RIGHT PANEL INSIDE')
    {
        $pdf->SetXY(165, 205);
        $pdf->Cell(14, 8.5, '', 1, 0, 'C');
    }

    if ($h['location'] == 'INSIDE' && $h['component'] == 'FLOOR')
    {
        $pdf->SetXY(151.5, 224.5);
        $pdf->Cell(19, 4.5, '', 1, 0, 'C');
    }

    if ($h['location'] == 'INSIDE' && $h['component'] == 'RIGHT DOOR INSIDE')
    {
        $pdf->SetXY(183.5, 199.5);
        $pdf->Cell(19, 4.5, '', 1, 0, 'C');
    }
}

//footer
$pdf->SetY(-28);
$pdf->SetFont('Arial', 'I', 9);
$pdf->Cell(170, 7, 'EIR SERIES No. : ' . $eir_no . " / SCI-ASDEKI / " . $bulan . " / " . $tahun . '', 0, 0, 'L');
$pdf->Cell(30, 7, 'Halaman ' . $pdf->PageNo() . '', 0, 0, 'C');

//Halaman 2
$pdf->AddPage('P', 'legal');
$pdf->SetY(5);
$pdf->Image(BASE_ASSET . 'img/logo/az_subtle.png', 14, 8);
$pdf->Image(BASE_ASSET . 'img/logo/scilogo.jpg', 175, 6);

$pdf->SetDrawColor(0, 0, 0);
$pdf->SetLineWidth(0.25);
$pdf->Cell(16);
$pdf->SetFont('Arial', 'B', 12);
$pdf->Line(77, 15, 155, 15);
$pdf->Cell(0, 15, 'EQUIPMENT INTERCHANGE REPORT', 0, 0, 'C');
$pdf->SetX(10);
$pdf->Cell(200, 33, '', 1, 0, 'C');
$pdf->SetX(10);
$pdf->SetFont('Arial', '', 9);
$pdf->Ln(18);
$pdf->Cell(70, 5, 'PT. GEMA NAWAGRAHA SEJATI (GNS)', 0, 0, 'L');
$pdf->SetFont('Arial', '', 15);
$pdf->SetTextColor(0, 0, 255);
if ($transaction['trx_type_id'] == 'EXIMP')
{
    $pdf->Cell(10, 10, 'X', 1, 0, 'C');
}
else
{
    $pdf->Cell(10, 10, '', 1, 0, 'C');
}
$pdf->SetFont('Arial', '', 9);
$pdf->SetTextColor(0, 0, 0);
$pdf->Cell(20, 10, 'Gate In', 1, 0, 'C');
$pdf->SetFont('Arial', '', 15);
$pdf->SetTextColor(0, 0, 255);
if ($transaction['trx_type_id'] == 'TOEXP')
{
    $pdf->Cell(10, 10, 'X', 1, 0, 'C');
}
else
{
    $pdf->Cell(10, 10, '', 1, 0, 'C');
}
$pdf->SetFont('Arial', '', 9);
$pdf->SetTextColor(0, 0, 0);
$pdf->Cell(20, 10, 'Gate Out', 1, 0, 'C');
//$pdf->Cell(40, 10, '', 1, 0, 'L');
$pdf->Cell(70, 5, 'Jln. Raya Pasar Minggu Kav.34', 0, 0, 'R');
$pdf->Ln();
$pdf->Cell(20, 5, 'Depo', 0, 0, 'L');
$pdf->Cell(5, 5, ':', 0, 0, 'L');
$pdf->Cell(45, 5, $trans['depo_id'], 0, 0, 'L');
$pdf->Cell(60, 5, '', 0, 0, 'L');
$pdf->Cell(70, 5, 'Jakarta Selatan 12780', 0, 0, 'R');
$pdf->Ln();
$pdf->Cell(70, 5, 'Member of ASDEKI', 0, 0, 'L');
$pdf->Cell(60, 5, '', 0, 0, 'L');
$pdf->Cell(70, 5, 'www.sucofindo.co.id', 0, 0, 'R');
$pdf->Ln();
$pdf->SetX(10);
$pdf->SetFont('Arial', "B", 12);
$pdf->Cell(200, 7, '5 SIDE CONTAINER PANEL FIGURES:', 1, 0, 'C');
$pdf->Ln();
$pdf->SetX(10);
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(67, 5, 'RIGHT SIDE PANEL', 1, 0, 'C');
$pdf->Cell(66, 5, 'LEFT SIDE PANEL', 1, 0, 'C');
$pdf->Cell(67, 5, 'TOP SIDE PANEL', 1, 0, 'C');
$pdf->Ln();
$pdf->Cell(67, 45, '', 1, 0, 'C');
$pdf->Cell(66, 45, '', 1, 0, 'C');
$pdf->Cell(67, 45, '', 1, 0, 'C');
$pdf->Ln();
$pdf->SetX(10);
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(30, 50, '', 1, 0, 'C');
$pdf->Cell(70, 5, 'DOOR PANEL', 1, 0, 'C');
$pdf->Cell(70, 5, 'INSIDE PANEL', 1, 0, 'C');
$pdf->Cell(30, 50, '', 1, 0, 'C');
$pdf->SetX(40);
$pdf->Cell(70, 50, '', 1, 0, 'C');
$pdf->Cell(70, 50, '', 1, 0, 'C');
if ($full['rightside'] != "")
    $pdf->Image(BASE_URL . 'android/files/images/full/' . $full['rightside'] . '', 16, 55, 55, 35);
if ($full['leftside'] != "")
    $pdf->Image(BASE_URL . 'android/files/images/full/' . $full['leftside'] . '', 82, 55, 55, 35);
if ($full['topside'] != "")
    $pdf->Image(BASE_URL . 'android/files/images/full/' . $full['topside'] . '', 150, 55, 55, 35);
if ($full['rearside'] != "")
    $pdf->Image(BASE_URL . 'android/files/images/full/' . $full['rearside'] . '', 47, 105, 55, 35);
if ($full['inside'] != "")
    $pdf->Image(BASE_URL . 'android/files/images/full/' . $full['inside'] . '', 118, 105, 55, 35);
$pdf->Ln();
$pdf->SetX(10);
$pdf->SetFont('Arial', "B", 12);
$pdf->Cell(200, 7, 'DAMAGE FIGURES:', 1, 0, 'C');
$pdf->Ln();
$pdf->SetX(10);
$pdf->SetFont('Arial', '', 7);
//Draw tabel
for ($p = 1; $p <= 3; $p++)
{
    $lebar = 200 / 3;
    $pdf->Cell($lebar, 5, "Details : ", 1, 0, 'L');
    $pdf->Cell($lebar, 5, "Details : ", 1, 0, 'L');
    $pdf->Cell($lebar, 5, "Details : ", 1, 0, 'L');
    $pdf->SetX(10);
    $pdf->Cell($lebar, 50, "", 1, 0, 'L');
    $pdf->Cell($lebar, 50, "", 1, 0, 'L');
    $pdf->Cell($lebar, 50, "", 1, 0, 'L');

    $pdf->Ln();
}
//Tulis Kerusakan
$index = 1;
foreach ($equip as $oi => $v)
{
    if ($index == 1 || $index == 4 || $index == 7)
        $x = 21;
    if ($index == 2 || $index == 5 || $index == 8)
        $x = 87;
    if ($index == 3 || $index == 6 || $index == 9)
        $x = 154;
    if ($index == 1 || $index == 2 || $index == 3)
        $y = 155;
    if ($index == 4 || $index == 5 || $index == 6)
        $y = 205;
    if ($index == 7 || $index == 8 || $index == 9)
        $y = 255;

    $damage = "";
    if ($v['bent_flag'] == "Y")
        $damage.=" -BT";
    if ($v['Dentet_flag'] == "Y")
        $damage.="-DT";
    if ($v['Leaking_flag'] == "Y")
        $damage.="-LK";
    if ($v['PushIn_flag'] == "Y")
        $damage.="-PI";
    if ($v['Broke_flag'] == "Y")
        $damage.="-BR";
    if ($v['Hole_flag'] == "Y")
        $damage.="-HO";
    if ($v['Missing_flag'] == "Y")
        $damage.="-MS";
    if ($v['PushOut_flag'] == "Y")
        $damage.="-PO";
    if ($v['Cut_flag'] == "Y")
        $damage.="-CU";
    if ($v['Loose_flag'] == "Y")
        $damage.="-LO";
    if ($v['Tom_flag'] == "Y")
        $damage.="-TM";
    if ($v['Rusty_flag'] == "Y")
        $damage.="-CO";
    $pdf->Text($x, $y, strtoupper(substr($v['location'], 0, 1)) . "_" . $v['component'] . "" . $damage);
    $index++;
}
$index = 1;
foreach ($equip as $oi => $v)
{
    if ($index == 1 || $index == 4 || $index == 7)
        $ximg = 16;
    if ($index == 2 || $index == 5 || $index == 8)
        $ximg = 83;
    if ($index == 3 || $index == 6 || $index == 9)
        $ximg = 149;
    if ($index == 1 || $index == 2 || $index == 3)
        $yimg = 160;
    if ($index == 4 || $index == 5 || $index == 6)
        $yimg = 210;
    if ($index == 7 || $index == 8 || $index == 9)
        $yimg = 260;

    $pdf->Image(BASE_URL . 'android/files/images/damage/' . $v['filename'], $ximg, $yimg, 55, 35);
    $index++;
}

//footer
$pdf->SetY(-28);
$pdf->SetFont('Arial', 'I', 9);
$pdf->Cell(170, 7, 'EIR SERIES No. : ' . $eir_no . " / SCI-ASDEKI / " . $bulan . " / " . $tahun . '', 0, 0, 'L');
$pdf->Cell(30, 7, 'Halaman ' . $pdf->PageNo() . '', 0, 0, 'C');

$pdf->Output();
?>
