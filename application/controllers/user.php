<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct()
    {

        parent::__construct();

        $this->load->model('User_Model');
        $this->load->library('lib_auth');
        $this->load->model('Log_Model');
    }

    public function index()
    {
        $this->lib_auth->check('yes');
        $data['page_title'] = APP_NAME . " | All Users";

        $data['datas'] = $this->User_Model->get_all_data("users");

        $this->load->view('data/all_user_v', $data);
    }

    public function add_new()
    {
        $data['page_title'] = APP_NAME . " | Add New Depo";

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('unique_id', 'Username', 'required');
        $data['error'] = "";
        
        if ($this->form_validation->run() == FALSE) {
            $data['error'] = validation_errors();
        } else {
            $cek = $this->User_Model->get_single("users", $this->input->post("unique_id"), "unique_id");
            if (!$cek) {
                $encriptionPassword = new \PyramidLib\helper\Authentication(SECRET_KEY);
                $passwordEncrypt = $encriptionPassword->encrypt_rijndael_256($this->input->post("password"));
                $encriptionPassword->close();

                $input = array(
                    "unique_id" => $this->input->post("unique_id"),
                    "name" => strtoupper($this->input->post("unique_id")),
                    "encrypted_password" => $passwordEncrypt,
                    "role_id" => $this->input->post("role_id"),
                    "depo_id" => $this->input->post("depo_id"),
                    "salt" => $this->input->post("password"),
                    "created_at" => date('y-m-d H:i:s'),
                );
                $record = $this->User_Model->insert("users", $input);
                if ($record) {
                    $data['success'] = 'Success Insert Data';
                } else {
                    $data['error'] = "Failed Insert Data";
                }
            }
        }
        $data['depos'] = $this->User_Model->get_all_data("depo");

        $this->load->view('data/add_new_user_v', $data);
    }

    public function save()
    {
        $input = array(
            "unique_id" => $this->input->post("unique_id"),
            "name" => ucfirst($this->input->post("unique_id")),
            "encrypted_password" => $this->input->post("password"),
            "role_id" => $this->input->post("role_id"),
            "depo_id" => $this->input->post("depo_id"),
            "salt" => md5($this->input->post("password"))
        );
        $edit = $this->input->post("edit");
        if ($edit)
        {
            $inputs = array(
                "name" => $this->input->post("name")
            );
            $id = $this->input->post("uid");
            $record = $this->User_Model->update("users", $id, $inputs, "uid");

            redirect(base_url() . "user");
        }
        else
        {
           redirect(base_url() . "user");
        }
    }

    public function delete()
    {
        $id_product = $this->uri->segment(3, '');

        if (isset($id_product))
        {
            $product = $this->User_Model->delete("users", str_replace("_", " ", $id_product), "uid");

            if ($product)
            {
                $this->User_Model->delete("users", $id_product, "uid");
                $this->Log_Model->delete("log_users", $id_product, "id_user");
                //$this->Log_Model->insert_log($_SESSION[SESSION_NAME]['uid'], "delete", "user");
            }
        }
        redirect(base_url() . "user");
    }

    public function view()
    {
        $id_product = $this->uri->segment(3, '');

        $data['edit'] = TRUE;
        if (isset($id_product))
        {
            $data['datas'] = $this->User_Model->get_single("users", $id_product, "uid");
        }
        else
        {
            redirect(base_url() . "user");
        }
        $data['page_title'] = APP_NAME . " | Edit Users";

        $this->load->library('form_validation');

        $this->load->view('data/edit_user_v', $data);
    }

    public function check_username()
    {
        $cek_email = $this->User_Model->check_username($this->input->post('unique_id'));

        if (!$cek_email) {
            echo "Your Unique_id already exist!";
        } else {
            echo 1;
        }
    }

    /* AJAX check valid password */
    function check_password()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', 'Password', 'required|matches[passconf]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');

        $this->form_validation->set_message('matches', 'Password tidak sama.');
        $this->form_validation->set_message('required', '%s tidak boleh kosong');

        if ($this->form_validation->run() == FALSE) {
            echo validation_errors();
        } else {
            echo 1;
        }
    }
}