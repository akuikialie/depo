<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('lib_auth');
        $this->load->model('User_Model');
        $this->load->model('Log_Model');
    }

    public function index()
    {
        $data['page'] = "Login";
        $this->load->view("login_v", $data);
    }

    public function login()
    {
        $data['page'] = "Login";
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $out = validation_errors();
        }
        else
        {
            $this->load->model('User_Model');

/*            $encriptionPassword = new \PyramidLib\helper\Authentication(SECRET_KEY);
            $passwordEncrypt = $encriptionPassword->encrypt_rijndael_256($password);
            $encriptionPassword->close();

            $hasil = $this->User_Model->cek_login($email, $passwordEncrypt);*/
	    $hasil = $this->User_Model->cek_login($email, $password);
            if ($hasil->status === 1)
            {
                //input log user
                $this->Log_Model->insert_login_users($hasil->user->uid);
                $this->lib_auth->save((array)$hasil->user);
                $out = 1;
            }else{
                $out=$hasil->error;
            }
        }
        echo $out;
    }

    public function logout()
    {
        //input log user
        $this->Log_Model->insert_logout_users($_SESSION[SESSION_NAME]['uid']);

        $this->lib_auth->logout();
    }

}
