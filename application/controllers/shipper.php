<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shipper extends CI_Controller {

    public function __construct()
    {

        parent::__construct();

        $this->load->model('Shipper_Model');
        $this->load->library('lib_auth');
        $this->load->model('Log_Model');
    }

    public function index()
    {
        $this->lib_auth->check('yes');
        $data['page_title'] = APP_NAME . " | All Shipper";

        if (isset($_GET['hal']))
            $hal = $_GET['hal'];
        else
            $hal = '';

        $dataPerhalaman = 10;
        ($hal == '') ? $nohalaman = 1 : $nohalaman = $hal;
        $offset = ($nohalaman - 1) * $dataPerhalaman;
        $off = abs((int) $offset);
        
        $jumlahData = count($this->Shipper_Model->get_all_data("shippers"));
        
        $data['paginator'] = $this->Shipper_Model->page($jumlahData, $dataPerhalaman, $hal);
        
        $data['datas'] = $this->Shipper_Model->get_all_data_tabel("shippers", $dataPerhalaman, $off);

        $this->load->view('data/all_shipper_v', $data);
    }

    public function add_new()
    {
        $data['page_title'] = APP_NAME . " | Add New Shipper";

        $this->load->library('form_validation');

        $this->load->view('data/add_new_shipper_v', $data);
    }

    public function save()
    {
        $input = array(
            "id" => str_replace(" ", "", strtolower($this->input->post("id"))),
            "name" => $this->input->post("name")
        );
        $edit = $this->input->post("edit");
        if ($edit)
        {
            $id = $this->input->post("id");
            $record = $this->Shipper_Model->update("shippers", $id, $input, "id");

        }
        else
        {

            $record = $this->Shipper_Model->insert("shippers", $input);
            if ($record)
            {
                redirect(base_url() . "shipper");
            }
        }
            redirect(base_url() . "shipper");
    }

    public function delete()
    {
        $id_product = $this->uri->segment(3, '');

        echo $id_product;
        if (isset($id_product))
        {
            $product = $this->Shipper_Model->delete("shippers", $id_product, "id");

            if ($product)
            {
                $this->Shipper_Model->delete("shippers", $id_product, "id");

            }
        }
                redirect(base_url() . "shipper");
    }

    public function view()
    {
        $id_product = $this->uri->segment(3, '');

        $data['edit'] = TRUE;
        if (isset($id_product))
        {
            $data['datas'] = $this->Shipper_Model->get_single("shippers", $id_product, "id");
        }
        else
        {
            redirect(base_url() . "shipper");
        }
        $data['page_title'] = APP_NAME . " | Edit Shipper";

        $this->load->library('form_validation');

        $this->load->view('data/edit_shipper_v', $data);
    }

}