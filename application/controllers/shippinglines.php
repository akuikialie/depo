<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shippinglines extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Shippingline_Model');
        $this->load->library('lib_auth');
        $this->load->model('Log_Model');
    }

    public function index()
    {
        $this->lib_auth->check('yes');
        $data['page_title'] = APP_NAME . " | All Shipper Lines";
        
        if (isset($_GET['hal']))
            $hal = $_GET['hal'];
        else
            $hal = '';

        $dataPerhalaman = 10;
        ($hal == '') ? $nohalaman = 1 : $nohalaman = $hal;
        $offset = ($nohalaman - 1) * $dataPerhalaman;
        $off = abs((int) $offset);
        
        $jumlahData = count($this->Shippingline_Model->get_all_data("shipping_lines"));
        
        $data['paginator'] = $this->Shippingline_Model->page($jumlahData, $dataPerhalaman, $hal);
        
        $data['datas'] = $this->Shippingline_Model->get_all_data_tabel("shipping_lines", $dataPerhalaman, $off);

        $this->load->view('data/all_shippinglines_v', $data);
    }

    public function add_new()
    {
        $data['page_title'] = APP_NAME . " | Add New Shipper Lines";

        $this->load->library('form_validation');

        $this->load->view('data/add_new_shippinglines_v', $data);
    }

    public function save()
    {
        $input = array(
            "Id" => str_replace(" ", "", strtoupper($this->input->post("id"))),
            "name" => $this->input->post("name"),
        );
        $edit = $this->input->post("edit");
        if ($edit)
        {
            $id = $this->input->post("id");
            $record = $this->Shippingline_Model->update("shipping_lines", $id, $input, "Id");

            redirect(base_url() . "shippinglines");
        }
        else
        {

            $record = $this->Shippingline_Model->insert("shipping_lines", $input);
            if ($record)
            {
                redirect(base_url() . "shippinglines");
            }
        }
            redirect(base_url() . "shippinglines");
    }

    public function delete()
    {
        $id_product = $this->uri->segment(3, '');

        if (isset($id_product))
        {
            $product = $this->Shippingline_Model->delete("shipping_lines", str_replace("_", " ", $id_product), "Id");

            if ($product)
            {
                $this->Shippingline_Model->delete("shipping_lines", $id_product, "id");

            }
        }
                redirect(base_url() . "shippinglines");
    }

    public function view()
    {
        $id_product = $this->uri->segment(3, '');

        $data['edit'] = TRUE;
        if (isset($id_product))
        {
            $data['datas'] = $this->Shippingline_Model->get_single("shipping_lines", str_replace("_", " ", $id_product), "Id");
        }
        else
        {
            redirect(base_url() . "shippinglines");
        }
        $data['page_title'] = APP_NAME . " | Edit Shipper Lines";

        $this->load->library('form_validation');

        $this->load->view('data/edit_shippinglines_v', $data);
    }

}