<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Truck extends CI_Controller {

    public function __construct()
    {

        parent::__construct();

        $this->load->model('Truck_Model');
        $this->load->library('lib_auth');
        $this->load->model('Log_Model');
    }

    public function index()
    {
        $this->lib_auth->check('yes');
        $data['page_title'] = APP_NAME . " | All Truck Company";
        
        if (isset($_GET['hal']))
            $hal = $_GET['hal'];
        else
            $hal = '';

        $dataPerhalaman = 10;
        ($hal == '') ? $nohalaman = 1 : $nohalaman = $hal;
        $offset = ($nohalaman - 1) * $dataPerhalaman;
        $off = abs((int) $offset);
        
        $jumlahData = count($this->Truck_Model->get_all_data("trucking_company"));
        
        $data['paginator'] = $this->Truck_Model->page($jumlahData, $dataPerhalaman, $hal);
        
        $data['datas'] = $this->Truck_Model->get_all_data_tabel("trucking_company", $dataPerhalaman, $off);

        $this->load->view('data/all_truck_v', $data);
    }

    public function add_new()
    {
        $data['page_title'] = APP_NAME . " | Add New Truck Company";

        $this->load->library('form_validation');

        $this->load->view('data/add_new_truck_v', $data);
    }

    public function save()
    {
        $input = array(
            "id" => str_replace(" ", "", strtolower($this->input->post("id"))),
            "name" => $this->input->post("name")
        );
        $edit = $this->input->post("edit");
        if ($edit)
        {
            $id = $this->input->post("id");
            $record = $this->Truck_Model->update("trucking_company", $id, $input, "id");

            redirect(base_url() . "truck");
        }
        else
        {
            $record = $this->Truck_Model->insert("trucking_company", $input);
            redirect(base_url() . "truck");
        }
    }

    public function delete()
    {
        $id_product = $this->uri->segment(3, '');

        if (isset($id_product))
        {
            $product = $this->Truck_Model->delete("trucking_company", $id_product, "id");

            if ($product)
            {
                $this->Truck_Model->delete("trucking_company", $id_product, "id");
            }
        }
        redirect(base_url() . "truck");
    }

    public function view()
    {
        $id_product = $this->uri->segment(3, '');

        $data['edit'] = TRUE;
        if (isset($id_product))
        {
            $data['datas'] = $this->Truck_Model->get_single("trucking_company", $id_product, "id");
        }
        else
        {
            redirect(base_url() . "truck");
        }
        $data['page_title'] = APP_NAME . " | Edit Truck Company";

        $this->load->library('form_validation');

        $this->load->view('data/edit_truck_v', $data);
    }

}