<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct()
    {

        parent::__construct();

        $this->load->model('User_Model');
        $this->load->library('lib_auth');
        $this->load->model('Log_Model');
    }

    public function index()
    {
        $this->lib_auth->check('yes');
        $data['page_title'] = APP_NAME . " | Setting";

        $id_user = $_SESSION[SESSION_NAME]['uid'];

        $data['profile'] = $this->User_Model->get_single("users", $id_user, "uid");

        $this->load->view('profile_v', $data);
    }

    public function add_new()
    {
        $data['page_title'] = APP_NAME . " | Add New Product";

        $this->load->library('form_validation');

        $this->load->view('data/add_new_menu_v', $data);
    }

    public function save()
    {
        $username = $this->input->post("username");
        $name = $this->input->post("name");
        $password = $this->input->post("password");
        $password2 = $this->input->post("password2");

        $usernames = $this->input->post("usernames");
        $names = $this->input->post("names");
        $passwords = $this->input->post("passwords");
        $password_texts = $this->input->post("password_texts");
        
        $id= $this->input->post("id_user");

        if (!empty($password) || !empty($password2))
        {
            if ($password != $password2)
            {
                redirect(base_url() . "profile");
            }
            else
            {
                if ($password != $password_texts)
                {
                    $input = array(
                        "encrypted_password" => $password,
                        "updated_at" => date("d-m-y h:i:s")
                    );
                    $password_change = $this->User_Model->update("users", $id, $input, "uid");
                    
                    if ($password_change)
                    {
                        redirect(base_url()."auth/logout");
                    }
                }
                else
                {
                    redirect(base_url() . "profile");
                }
            }
        }
        if ($name != $names)
        {
            $input = array(
                "name" => $name
            );
            $user_change = $this->User_Model->update("users", $id, $input, "id_user");

            if ($user_change)
            {
            }
        }
                redirect(base_url() . "profile");
    }

    public function delete()
    {
        $id_product = $this->uri->segment(3, '');

        if (isset($id_product))
        {
            $product = $this->Menu_Model->get_single("menus", $id_product, "id_menu");

            if ($product)
            {
                $this->Menu_Model->delete("menus", $id_product, "id_menu");
                $this->Menu_Model->delete("menu_products", $id_product, "id_menu");

                redirect(base_url() . "menu");
            }
        }
    }

    public function view()
    {
        $id_product = $this->uri->segment(3, '');

        $data['edit'] = TRUE;
        if (isset($id_product))
        {
            $data['datas'] = $this->Menu_Model->get_single("menus", $id_product, "id_menu");
        }
        else
        {
            redirect(base_url() . "menu");
        }
        $data['page_title'] = APP_NAME . " | Add New Menu";

        $this->load->library('form_validation');

        $this->load->view('data/edit_menu_v', $data);
    }

}