<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct()
    {

        parent::__construct();

        $this->load->model('User_Model');
    }

    public function index()
    {
        $data['page_title'] = APP_NAME . " | All Users";

        $data['datas'] = $this->User_Model->get_all_data("users");

        $this->load->view('data/all_users_v', $data);
    }

    public function add_new()
    {
        $data['page_title'] = APP_NAME . " | Add New Users";

        $this->load->library('form_validation');

        $this->load->view('data/add_new_users_v', $data);
    }

    public function save()
    {
        $input = array(
            "unique_id" => $this->input->post("unique_id"),
            "name" => $this->input->post("name"),
			"email" => $this->input->post("email"),
			"depo_id" => $this->input->post("depo_id"),
			"created_at" => date("Y-m-d H:i:s"),
			"encrypted_password" => $this->input->post("encrypted_password"),
			"role_id" => $this->input->post("role_id")
        );
        $edit = $this->input->post("edit");
        if ($edit)
        {
            $id = $this->input->post("id");
            $record = $this->User_Model->update("users", $id, $input, "uid");
            redirect(base_url() . "users");
        }
        else
        {

            $record = $this->User_Model->insert("users", $input);
            if ($record)
            {
                redirect(base_url() . "users");
            }
        }
    }

    public function delete()
    {
        $id_product = $this->uri->segment(3, '');

        if (isset($id_product))
        {
            $product = $this->User_Model->delete("users", str_replace("_", " ",$id_product), "uid");

            if ($product)
            {
                $this->User_Model->delete("users", $id_product, "uid");

                redirect(base_url() . "users");
            }
        }
    }

    public function view()
    {
        $id_product = $this->uri->segment(3, '');

        $data['edit'] = TRUE;
        if (isset($id_product))
        {
            $data['datas'] = $this->User_Model->get_single("users",  str_replace("_", " ",$id_product), "uid");
        }
        else
        {
            redirect(base_url() . "users");
        }
        $data['page_title'] = APP_NAME . " | Edit users";

        $this->load->library('form_validation');

        $this->load->view('data/edit_users_v', $data);
    }

}