<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Setting extends CI_Controller {

    public function __construct()
    {

        parent::__construct();

        $this->load->model('Config_Model');
        $this->load->library('lib_auth');
        $this->load->model('Log_Model');
    }

    public function index()
    {
        $this->lib_auth->check('yes');
        $data['page_title'] = APP_NAME . " | All Setting";

        $data['datas'] = $this->Config_Model->get_all_data("option");

        $this->load->view('data/all_setting_v', $data);
    }

    public function add_new()
    {
        $data['page_title'] = APP_NAME . " | Add New Depo";

        $this->load->library('form_validation');

        $this->load->view('data/add_new_depo_v', $data);
    }

    public function save()
    {
        $input = array(
            "option_value" => $this->input->post("option_value"),
        );
        $edit = $this->input->post("edit");
        if ($edit)
        {
            $id = $this->input->post("id");
            $record = $this->Config_Model->update("option", $id, $input, "id_option");

            //$this->Log_Model->insert_log($_SESSION[SESSION_NAME]['uid'], "update", "option");
            redirect(base_url() . "setting");
        }
    }

    public function view()
    {
        $id_product = $this->uri->segment(3, '');

        $data['edit'] = TRUE;
        if (isset($id_product))
        {
            $data['datas'] = $this->Config_Model->get_single("option", $id_product, "id_option");
        }
        else
        {
            redirect(base_url() . "depo");
        }
        $data['page_title'] = APP_NAME . " | Edit Setting";

        $this->load->library('form_validation');

        $this->load->view('data/edit_setting_v', $data);
    }

    public function tarif()
    {
        $data['page_title'] = APP_NAME . " | All Setting Tarif";

        $data['datas'] = $this->Config_Model->get_all_data("tarrif");

        $this->load->view('data/all_tarif_v', $data);
    }

    public function view_tarif()
    {
        $data['page_title'] = APP_NAME . " | All Setting Tarif";
        $id_product = $this->uri->segment(3, '');

        $data['datas'] = $this->Config_Model->get_single("tarrif", $id_product, "id");
        $data['edit'] = TRUE;

        $this->load->view('data/det_tarif_v', $data);
    }

    public function save_tarif()
    {
        $input = array(
            "tarrif_rate" => $this->input->post("tarif"),
        );
        $edit = $this->input->post("edit");
        if ($edit)
        {
            $id = $this->input->post("id");
            $record = $this->Config_Model->update("tarrif", $id, $input, "id");
            
            //$this->Log_Model->insert_log($_SESSION[SESSION_NAME]['uid'], "update", "tarif");
        }
        redirect(base_url() . "setting/tarif");
    }

}