<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Log extends CI_Controller {

    public function __construct()
    {

        parent::__construct();

        $this->load->model('User_Model');
        $this->load->library('lib_auth');
        $this->load->model('Log_Model');
    }

    public function user()
    {
        $this->lib_auth->check('yes');
        $data['page_title'] = APP_NAME . " | All Log User";

        $data['datas'] = $this->Log_Model->get_all_data("users");

        $this->load->view('data/all_log_user_v', $data);
    }

    public function view_log_user()
    {
        $data['page_title'] = APP_NAME . " | All Log User";
        
        $uid = $this->uri->segment(3, '');
        
        
        $data['datas'] = $this->Log_Model->get_datas("log_users", $uid, "id_user");
        
        $this->load->view('data/det_log_user_v', $data);
    }
    
    public function input()
    {
        $this->lib_auth->check('yes');
        $data['page_title'] = APP_NAME . " | All Log User Input";

        $data['datas'] = $this->Log_Model->get_all_data("users");

        $this->load->view('data/all_log_user_input_v', $data);
    }

    public function view_log_user_input()
    {
        $data['page_title'] = APP_NAME . " | All Log User Input";
        $uid = $this->uri->segment(3, '');
        
        $data['datas'] = $this->Log_Model->get_datas("log_input", $uid, "unique_id");
        
        $this->load->view('data/det_log_input_user_v', $data);
    }

    public function add_new()
    {
        $data['page_title'] = APP_NAME . " | Add New Depo";

        $this->load->library('form_validation');

        $this->load->view('data/add_new_user_v', $data);
    }

    public function save()
    {
        $input = array(
            "id" => $this->input->post("id"),
            "name" => $this->input->post("name"),
            'address_line_1' => $this->input->post("address")
        );
        $edit = $this->input->post("edit");
        if ($edit)
        {
            $id = $this->input->post("id");
            $record = $this->User_Model->update("depo", $id, $input, "id");

            redirect(base_url() . "depo");
        }
        else
        {

            $record = $this->User_Model->insert("depo", $input);
            if ($record)
            {
            }
        }
                redirect(base_url() . "depo");
    }

    public function delete()
    {
        $id_product = $this->uri->segment(3, '');

        if (isset($id_product))
        {
            $product = $this->User_Model->delete("depo", str_replace("_", " ",$id_product), "id");

            if ($product)
            {
                $this->Depo_Model->delete("depo", $id_product, "id");

            }
        }
                redirect(base_url() . "depo");
    }

    public function view()
    {
        $id_product = $this->uri->segment(3, '');

        $data['edit'] = TRUE;
        if (isset($id_product))
        {
            $data['datas'] = $this->User_Model->get_single("users",  $id_product, "uid");
        }
        else
        {
            redirect(base_url() . "user");
        }
        $data['page_title'] = APP_NAME . " | Edit Users";

        $this->load->library('form_validation');

        $this->load->view('data/edit_user_v', $data);
    }

}