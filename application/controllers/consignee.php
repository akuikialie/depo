<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Consignee extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Consignee_Model');
        $this->load->library('lib_auth');
        $this->load->model('Log_Model');
    }

    public function index()
    {
        $this->lib_auth->check('yes');
        $data['page_title'] = APP_NAME . " | All Consignee";

        if (isset($_GET['hal']))
            $hal = $_GET['hal'];
        else
            $hal = '';

        $dataPerhalaman = 10;
        ($hal == '') ? $nohalaman = 1 : $nohalaman = $hal;
        $offset = ($nohalaman - 1) * $dataPerhalaman;
        $off = abs((int) $offset);
        
        $jumlahData = count($this->Consignee_Model->get_all_data("consignee"));
        
        $data['paginator'] = $this->Consignee_Model->page($jumlahData, $dataPerhalaman, $hal);
        
        $data['datas'] = $this->Consignee_Model->get_all_data_tabel("consignee", $dataPerhalaman, $off);

        $this->load->view('data/all_consignee_v', $data);
    }

    public function add_new()
    {
        $data['page_title'] = APP_NAME . " | Add New Consignee";

        $this->load->library('form_validation');

        $this->load->view('data/add_new_consignee_v', $data);
    }

    public function save()
    {
        $input = array(
            "id" => $this->input->post("id"),
            "name" => $this->input->post("name")
        );
        $edit = $this->input->post("edit");
        if ($edit)
        {
            $id = $this->input->post("id");
            $record = $this->Consignee_Model->update("consignee", $id, $input, "id");

            //$this->Log_Model->insert_log($_SESSION[SESSION_NAME]['uid'], "update", "consignee", $input);
        }
        else
        {
            $record = $this->Consignee_Model->insert("consignee", $input);
            //$this->Log_Model->insert_log($_SESSION[SESSION_NAME]['uid'], "add", "consignee",$input);
        }
        redirect(base_url() . "consignee");
    }

    public function delete()
    {
        $id_product = $this->uri->segment(3, '');

        echo $id_product;
        if (isset($id_product))
        {
            $product = $this->Consignee_Model->delete("consignee", $id_product, "id");

            if ($product)
            {
                $this->Consignee_Model->delete("consignee", $id_product, "id");
                $this->Log_Model->insert_log($_SESSION[SESSION_NAME]['uid'], "delete", "consignee", $id_product);
            }
        }
        redirect(base_url() . "consignee");
    }

    public function view()
    {
        $id_product = $this->uri->segment(3, '');

        $data['edit'] = TRUE;
        if (isset($id_product))
        {
            $data['datas'] = $this->Consignee_Model->get_single("consignee", $id_product, "id");
        }
        else
        {
            redirect(base_url() . "consignee");
        }
        $data['page_title'] = APP_NAME . " | Edit Consignee";

        $this->load->library('form_validation');

        $this->load->view('data/edit_consignee_v', $data);
    }

}