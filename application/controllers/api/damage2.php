<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once ( APPPATH . '/libraries/REST_Controller.php');

class Damage extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Damage_Model", "", TRUE);
        $this->load->library('lib_rest');
        $this->load->model('Equipment_Model');
    }

    public function add_damage_post()
    {
        $eq_nbr = $this->post("eq_nbr");

        $bent = $this->post("bent");
        $dentet = $this->post("dentet");
        $leaking = $this->post("leaking");
        $pushin = $this->post("pushin");
        $pushout = $this->post("pushout");
        $broke = $this->post("broke");
        $hole = $this->post("hole");
        $missing = $this->post("missing");
        $cut = $this->post("cut");
        $loose = $this->post("loose");
        $tom = $this->post("tom");
        $rusty = $this->post("rusty");

        $filenaname = "";
        $filenaname.=date("dmY") . "-".$eq_nbr."-" . $this->post("location") . "-" . $this->post("component") . "-";

        if (!empty($bent))
            $filenaname.="BT";
        if (!empty($dentet))
            $filenaname.="DT";
        if (!empty($leaking))
            $filenaname.="LA";
        if (!empty($pushin))
            $filenaname.="PI";
        if (!empty($pushout))
            $filenaname.="PO";
        if (!empty($broke))
            $filenaname.="BR";
        if (!empty($hole))
            $filenaname.="HO";
        if (!empty($missing))
            $filenaname.="MS";
        if (!empty($cut))
            $filenaname.="CU";
        if (!empty($loose))
            $filenaname.="LO";
        if (!empty($tom))
            $filenaname.="TO";
        if (!empty($rusty))
            $filenaname.="CO";


        $data = array(
            "equse_gkey" => "",
            "component" => $this->post("component"),
            "location" => $this->post("location"),
            "location_component" => $this->post("location") . $this->post("component"),
            "survey_location" => $this->post("survey_location"),
            "bent_flag" => !empty($bent) ? $bent : "",
            "Dentet_flag" => !empty($dentet) ? $dentet : "N",
            "Leaking_flag" => !empty($leaking) ? $leaking : "N",
            "PushIn_flag" => !empty($pushin) ? $pushin : "N",
            "PushOut_flag" => !empty($pushout) ? $pushout : "N",
            "Broke_flag" => !empty($broke) ? $broke : "N",
            "Hole_flag" => !empty($hole) ? $hole : "N",
            "Missing_flag" => !empty($missing) ? $missing : "N",
            "Cut_flag" => !empty($cut) ? $cut : "N",
            "Loose_flag" => !empty($loose) ? $loose : "N",
            "Tom_flag" => !empty($tom) ? $tom : "N",
            "Rusty_flag" => !empty($rusty) ? $rusty : "N",
            "creator" => $this->post("creator"),
            "created" => date("Y-m-d H:i:s"),
            "depo_id" => $this->post("depo_id"),
            "eq_nbr" => $this->post("eq_nbr"),
            "id" => $filenaname,
            "photoinstring" => "",
            "filename" => "",
        );
		
		$cek=$this->Damage_Model->cek("equipment_damages", "eq_nbr", "component", $this->post("eq_nbr"), $this->post("component") );
		if($cek){
			$record=$this->Damage_Model->update_s("equipment_damages", "eq_nbr", "component", $this->post("eq_nbr"), $this->post("component") );
		}
		else{
			$record = $this->Damage_Model->insert("equipment_damages", $data);
		}
        if ($record)
        {
//                $temp = array(
//                    "eq_nbr" => $this->post("eq_nbr"),
//                    "depo_id" => $this->post("depo_id"),
//                    "ready_to_eir_in" => "Y",
//                    "ready_to_eir_out" => "",
//                );
//
//                $this->Damage_Model->insert("equipment_temp", $temp);

            if (!empty($_FILES))
            {
                $this->load->library('Imaging');
                //$targetPath = $_SERVER['DOCUMENT_ROOT'] . "/" . APP_SHORT_NAME . '/android/files/images';
				$targetPath = $_SERVER['DOCUMENT_ROOT']  . '/android/files/images/damage';

                $handle = new Imaging();
                $handle->upload($_FILES['filename']);
                if ($handle->uploaded)
                {
                    $handle->file_src_name_body = $filenaname; // hard name
                    $handle->file_overwrite = true;
                    $handle->file_auto_rename = false;
                    $handle->file_max_size = '9999990'; // max size
                    $handle->jpeg_quality = 4500;
                    $handle->Process($targetPath . '/');

                    if ($handle->processed)
                    {
                        $this->Damage_Model->update("equipment_damages", $record, array("filename" => $handle->file_dst_name), "id_damage");
                    }
                    $handle->clean();
                }
            }
            $this->response(
                    array("status" => 1, "detail" => $data)
            );
        }
        else
        {
            $this->response(
                    array("status" => 0, "message" => "Failed Add")
            );
        }
    }

    public function add_foto_post()
    {
        $eq_nbr = $this->post("eq_nbr");
        $inside = $this->post("inside");
        $rightside = $this->post("rightside");
        $leftside = $this->post("leftside");
        $bottomside = $this->post("bottomside");
        $frontside = $this->post("frontside");
        $rearside = $this->post("rearside");
        $topside = $this->post("topside");

        $data = array(
            "eq_nbr" => $eq_nbr,
            "equse_gkey" => "",
            "depo_id" => $this->post("depo_id"),
            /*"leftside" => !empty($leftside) ? $leftside : "N",
            "rightside" => !empty($rightside) ? $rightside : "N",
            "topside" => !empty($topside) ? $topside : "N",
            "bottomside" => !empty($bottomside) ? $bottomside : "N",
            "frontside" => !empty($leftside) ? $leftside : "N",
            "rearside" => !empty($rearside) ? $rearside : "N",
            "inside" => !empty($inside) ? $inside : "N"*/
        );

        $record = $this->Damage_Model->insert("equipment_foto", $data);
        if ($record)
        {
            $data = array(
                "eq_nbr" => $eq_nbr,
                "depo_id" => $this->post("depo_id"),
                "ready_to_eir_in" => "Y",
                "survey_pos_id" => $this->post("survey_pos_id"),
                "ready_to_eir_out" => $this->post("ready_to_eir_out"),
            );

            $this->Damage_Model->insert("equipment_temp", $data);

            $bag = array("leftside", "rightside", "topside", "bottomside", "frontside", "rearside", "inside");
            $this->load->library('Imaging');

			$targetPath = $_SERVER['DOCUMENT_ROOT']  . '/android/files/images/full';
			
            foreach ($bag as $v)
            {
				if(!empty($_FILES[$v])){
					$filenaname =date("dmY") . "-".$eq_nbr."-".$v. "-".$record;
				
					$handle = new Imaging();
					$handle->upload($_FILES[$v]);
					if ($handle->uploaded)
					{
						$handle->file_src_name_body = $filenaname; // hard name
						$handle->file_overwrite = true;
						$handle->file_auto_rename = false;
						$handle->file_max_size = '9999990'; // max size
						$handle->jpeg_quality = 4500;
						$handle->Process($targetPath . '/');

						if ($handle->processed)
						{
							$this->Damage_Model->update("equipment_foto", $record, array($v => $handle->file_dst_name), "id");
						}
						$handle->clean();
					}
				}
            }
            $this->response(
                    array("status" => 1)
            );
        }
        else
        {
            $this->response(
                    array("status" => 0)
            );
        }
    }

    public function get_equipment_get()
    {
        $user = $this->Equipment_Model->get_datas("equipment_uses", "PREADVISE", "ctr_position");

        $out = array();
        foreach ($user as $v)
        {
            $out[] = array(
                "eq_nbr" => $v['eq_nbr'],
                "depo_id" => $v['depo_id'],
                "is_code" => $v['iso_code'],
                "eq_type" => $v['eq_type'],
                "eq_size" => $v['eq_size']
            );
        }
        $this->response(
                array("equipment" => $out)
        );
    }

}

/* End of file user.php */
/* Location: ./application/controllers/api/user.php */