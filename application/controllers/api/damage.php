<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once ( APPPATH . '/libraries/REST_Controller.php');

class Damage extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Damage_Model", "", TRUE);
        $this->load->library('lib_rest');
        $this->load->model('Log_Model');
        $this->load->model('Equipment_Model');
    }

    public function add_damage_post()
    {
        $outs = array();
        $eq_nbr = $this->post("eq_nbr");
        $depo_id = $this->post("depo_id");
        $survey_location = $this->post("survey_location");
        $component = $this->post("component");

        $eq_nbr = trim($eq_nbr);

        if (!isset($eq_nbr) || empty($eq_nbr)) {
            $this->response(array("status" => 0, "error" => "Paramater is not valid 'eq_nbr'"));
            return;
        }

        if (!isset($depo_id) || empty($depo_id)) {
            $this->response(array("status" => 0, "error" => "Paramater is not valid 'depo_id'"));
            return;
        }

        if (!isset($survey_location) || empty($survey_location)) {
            $this->response(array("status" => 0, "error" => "Paramater is not valid 'survey_location'"));
            return;
        }

        if (!isset($component) || empty($component)) {
            $this->response(array("status" => 0, "error" => "Paramater is not valid 'component'"));
            return;
        }

        $eq_nbr = str_replace(" ", "", $eq_nbr);

        $bent = $this->post("bent");
        $dentet = $this->post("dentet");
        $leaking = $this->post("leaking");
        $pushin = $this->post("pushin");
        $pushout = $this->post("pushout");
        $broke = $this->post("broke");
        $hole = $this->post("hole");
        $missing = $this->post("missing");
        $cut = $this->post("cut");
        $loose = $this->post("loose");
        $tom = $this->post("tom");
        $rusty = $this->post("rusty");

        $filenaname = "";
        $filenaname.=date("dmY") . "-" . $eq_nbr . "-" . $this->post("location") . "-" . $this->post("component") . "-";

        if ($bent == "Y")
            $filenaname.="BT";
        if ($dentet == "Y")
            $filenaname.="DT";
        if ($leaking == "Y")
            $filenaname.="LA";
        if ($pushin == "Y")
            $filenaname.="PI";
        if ($pushout == "Y")
            $filenaname.="PO";
        if ($broke == "Y")
            $filenaname.="BR";
        if ($hole == "Y")
            $filenaname.="HO";
        if ($missing == "Y")
            $filenaname.="MS";
        if ($cut == "Y")
            $filenaname.="CU";
        if ($loose == "Y")
            $filenaname.="LO";
        if ($tom == "Y")
            $filenaname.="TO";
        if ($rusty == "Y")
            $filenaname.="CO";

        $data = array(
            "equse_gkey" => "",
            "component" => $this->post("component"),
            "location" => $this->post("location"),
            "location_component" => $this->post("location") . $this->post("component"),
            "survey_location" => $this->post("survey_location"),
            "bent_flag" => !empty($bent) ? $bent : "N",
            "Dentet_flag" => !empty($dentet) ? $dentet : "N",
            "Leaking_flag" => !empty($leaking) ? $leaking : "N",
            "PushIn_flag" => !empty($pushin) ? $pushin : "N",
            "PushOut_flag" => !empty($pushout) ? $pushout : "N",
            "Broke_flag" => !empty($broke) ? $broke : "N",
            "Hole_flag" => !empty($hole) ? $hole : "N",
            "Missing_flag" => !empty($missing) ? $missing : "N",
            "Cut_flag" => !empty($cut) ? $cut : "N",
            "Loose_flag" => !empty($loose) ? $loose : "N",
            "Tom_flag" => !empty($tom) ? $tom : "N",
            "Rusty_flag" => !empty($rusty) ? $rusty : "N",
            "creator" => $this->post("creator"),
            "created" => date("Y-m-d H:i:s"),
            "depo_id" => $this->post("depo_id"),
            "eq_nbr" => $eq_nbr,
            "id" => "",
            "photoinstring" => "",
        );

        $location_component = $this->post("location") . $this->post("component");

        $cek_temp = $this->Damage_Model->get_single("equipment_temp", $eq_nbr, "eq_nbr");
        if ($cek_temp) {
            $cek_damage = $this->Damage_Model->check_damage($eq_nbr, $location_component);
            if (!$cek_damage) {
                $record = $this->Damage_Model->insert("equipment_damages", $data);
                if (!empty($_FILES)) {
                    $this->load->library('Imaging');
                    $targetPath = $_SERVER['DOCUMENT_ROOT'] . "/" . BASE_URL_IMAGE_DAMAGE;

                    $handle = new Imaging();
                    $handle->upload($_FILES['filename']);
                    if ($handle->uploaded) {
                        $handle->file_src_name_body = $filenaname; // hard name
                        $handle->file_overwrite = true;
                        $handle->file_auto_rename = false;
                        $handle->file_max_size = '9999990'; // max size
                        $handle->jpeg_quality = 4500;
                        $handle->Process($targetPath . '/');

                        if ($handle->processed) {
                            $data['filename'] = $handle->file_dst_name;
                            $imgData = base64_encode(file_get_contents($targetPath . "/" . $data['filename']));

                            $this->Damage_Model->update("equipment_damages", $record, array("filename" => $handle->file_dst_name, 'photoinstring' => $imgData), "id_damage");
                        }
                        $handle->clean();
                    }
                }
                $outs = array("status" => 1, "detail" => $data);
            } else {
                $record = $this->Damage_Model->update("equipment_damages", $eq_nbr, $data, 'eq_nbr');
                if (!empty($_FILES)) {
                    $this->load->library('Imaging');
                    $targetPath = $_SERVER['DOCUMENT_ROOT'] . "/" . BASE_URL_IMAGE_DAMAGE;

                    $handle = new Imaging();
                    $handle->upload($_FILES['filename']);
                    if ($handle->uploaded) {
                        $handle->file_src_name_body = $filenaname; // hard name
                        $handle->file_overwrite = true;
                        $handle->file_auto_rename = false;
                        $handle->file_max_size = '9999990'; // max size
                        $handle->jpeg_quality = 4500;
                        $handle->Process($targetPath . '/');

                        if ($handle->processed) {
                            $data['filename'] = $handle->file_dst_name;
                            // Read image path, convert to base64 encoding
                            $imgData = base64_encode(file_get_contents($targetPath . "/" . $data['filename']));

                            $this->Damage_Model->update("equipment_damages", $record, array("filename" => $handle->file_dst_name, 'photoinstring' => $imgData), "id_damage");
                        }
                        $handle->clean();
                    }
                }
                $outs = array("status" => 1, "detail" => $data);
            }
        } else {
            $data_temp = array(
                "eq_nbr" => $eq_nbr,
                "depo_id" => $this->post("depo_id"),
                "ready_to_eir_in" => "N",
                "survey_pos_id" => $this->post("survey_location"),
                "ready_to_eir_out" => "N",
            );

            $this->Damage_Model->insert("equipment_temp", $data_temp);

            $cek_damage = $this->Damage_Model->check_damage($eq_nbr, $location_component);
            if (!$cek_damage) {
                $record = $this->Damage_Model->insert("equipment_damages", $data);
                if ($record) {
                    if (!empty($_FILES)) {
                        $this->load->library('Imaging');
                        $targetPath = $_SERVER['DOCUMENT_ROOT'] . "/" . BASE_URL_IMAGE_DAMAGE;

                        $handle = new Imaging();
                        $handle->upload($_FILES['filename']);
                        if ($handle->uploaded) {
                            $handle->file_src_name_body = $filenaname; // hard name
                            $handle->file_overwrite = true;
                            $handle->file_auto_rename = false;
                            $handle->file_max_size = '9999990'; // max size
                            $handle->jpeg_quality = 4500;
                            $handle->Process($targetPath . '/');

                            if ($handle->processed) {
                                $data['filename'] = $handle->file_dst_name;
                                // Read image path, convert to base64 encoding
                                $imgData = base64_encode(file_get_contents($targetPath . "/" . $data['filename']));

                                $this->Damage_Model->update("equipment_damages", $record, array("filename" => $handle->file_dst_name, 'photoinstring' => $imgData), "id_damage");
                            }
                            $handle->clean();
                        }
                    }
                    $outs = array("status" => 1, "detail" => $data);
                } else {
                    $outs = array("status" => 0, "error" => "Failed");
                }
            } else {
                $record = $this->Damage_Model->update("equipment_damages", $eq_nbr, $data, 'eq_nbr');
                if ($record) {
                    if (!empty($_FILES)) {
                        $this->load->library('Imaging');
                        $targetPath = $_SERVER['DOCUMENT_ROOT'] . "/" . BASE_URL_IMAGE_DAMAGE;

                        $handle = new Imaging();
                        $handle->upload($_FILES['filename']);
                        if ($handle->uploaded) {
                            $handle->file_src_name_body = $filenaname; // hard name
                            $handle->file_overwrite = true;
                            $handle->file_auto_rename = false;
                            $handle->file_max_size = '9999990'; // max size
                            $handle->jpeg_quality = 4500;
                            $handle->Process($targetPath . '/');

                            if ($handle->processed) {
                                $data['filename'] = $handle->file_dst_name;
                                // Read image path, convert to base64 encoding
                                $imgData = base64_encode(file_get_contents($targetPath . "/" . $data['filename']));

                                $this->Damage_Model->update("equipment_damages", $record, array("filename" => $handle->file_dst_name, 'photoinstring' => $imgData), "id_damage");
                            }
                            $handle->clean();
                        }
                    }
                    $outs = array("status" => 1, "detail" => $data);
                } else {
                    $outs = array("status" => 0, "error" => "Failed");
                }
            }
        }
        $this->response($outs);
    }

    public function add_foto_post()
    {
        $eq_nbr = $this->post("eq_nbr");
        $depo_id = $this->post("depo_id");
        $inside = $this->post("inside");
        $rightside = $this->post("rightside");
        $leftside = $this->post("leftside");
        $bottomside = $this->post("bottomside");
        $frontside = $this->post("frontside");
        $rearside = $this->post("rearside");
        $topside = $this->post("topside");

        if (!isset($eq_nbr) || empty($eq_nbr)) {
            $this->response(array("status" => 0, "error" => "Paramater is not valid 'eq_nbr'"));
            return;
        }

        if (!isset($depo_id) || empty($depo_id)) {
            $this->response(array("status" => 0, "error" => "Paramater is not valid 'depo_id'"));
            return;
        }

        $data = array(
            "eq_nbr" => $eq_nbr,
            "equse_gkey" => "",
            "depo_id" => $depo_id,
            "leftside" => !empty($leftside) ? $leftside : "",
            "rightside" => !empty($rightside) ? $rightside : "",
            "topside" => !empty($topside) ? $topside : "",
            "bottomside" => !empty($bottomside) ? $bottomside : "",
            "frontside" => !empty($leftside) ? $leftside : "",
            "rearside" => !empty($rearside) ? $rearside : "",
            "inside" => !empty($inside) ? $inside : ""
        );

        $record = $this->Damage_Model->insert("equipment_foto", $data);
        if ($record) {
            $records = $this->Damage_Model->get_single("equipment_temp", $eq_nbr, "eq_nbr");
            if (!$records) {
                $data = array(
                    "eq_nbr" => $eq_nbr,
                    "depo_id" => $this->post("depo_id"),
                    "ready_to_eir_in" => "Y",
                    "survey_pos_id" => $this->post("survey_pos_id"),
                    "ready_to_eir_out" => $this->post("ready_to_eir_out"),
                );

                $this->Damage_Model->insert("equipment_temp", $data);
            } else {
                $this->Damage_Model->update("equipment_temp", $eq_nbr, array("ready_to_eir_in" => "Y"), "eq_nbr");
            }

            $rec = $this->Damage_Model->get_single("equipment_uses", $eq_nbr, "eq_nbr");
            if ($rec) {
                $this->Damage_Model->update("equipment_uses", $eq_nbr, array("ready_to_print_eir" => "Y"), "eq_nbr");
            }

            $bag = array("leftside", "rightside", "topside", "bottomside", "frontside", "rearside", "inside");
            $this->load->library('Imaging');

            $targetPath = $_SERVER['DOCUMENT_ROOT'] . "/" . BASE_URL_IMAGE_FULL;

            foreach ($bag as $v) {
                if (isset($_FILES[$v])) {
                    $handle = new Imaging();
                    $handle->upload($_FILES[$v]);
                    if ($handle->uploaded) {
                        $filenaname = date("dmY") . "-" . $eq_nbr . "-" . $v . "-" . $record;
                        $handle->file_src_name_body = $filenaname;
                        $handle->file_overwrite = true;
                        $handle->file_auto_rename = false;
                        $handle->file_max_size = '9999990'; // max size
                        $handle->jpeg_quality = 4500;
                        $handle->Process($targetPath . '/');

                        if ($handle->processed) {
                            $this->Damage_Model->update("equipment_foto", $record, array($v => $handle->file_dst_name), "id");
                        }
                        $handle->clean();
                    }
                }
            }
            $this->response(
                    array("status" => 1)
            );
        } else {
            $this->response(
                    array("status" => 0)
            );
        }
    }

    public function get_equipment_get()
    {
        $user = $this->Equipment_Model->get_datas("equipment_uses", "PREADVISE", "ctr_position");

        $out = array();
        foreach ($user as $v)
        {
            $out[] = array(
                "eq_nbr" => $v['eq_nbr'],
                "depo_id" => $v['depo_id'],
                "is_code" => $v['iso_code'],
                "eq_type" => $v['eq_type'],
                "eq_size" => $v['eq_size']
            );
        }
        $this->response(
                array("equipment" => $out)
        );
    }

}

/* End of file user.php */
/* Location: ./application/controllers/api/user.php */