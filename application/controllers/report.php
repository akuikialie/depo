<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report extends CI_Controller {

    public function __construct()
    {

        parent::__construct();

        $this->load->model('Depo_Model');
        $this->load->library('lib_auth');
        $this->load->model('Log_Model');
        $this->load->model('Equipment_Model');
    }

    public function index()
    {
        $this->lib_auth->check('yes');
        $data['page_title'] = APP_NAME . " | All Depo";

        $data['datas'] = $this->Depo_Model->get_all_data("depo");

        $this->load->view('data/all_depo_v', $data);
    }

    public function depo()
    {
        $data['page_title'] = APP_NAME . " | Report Depo";
     
        if (isset($_GET['hal']))
            $hal = $_GET['hal'];
        else
            $hal = '';
        
        if (isset($_GET['month']))
            $month = $_GET['month'];
        else
            $month = '';
        
        if (isset($_GET['year']))
            $year = $_GET['year'];
        else
            $year = '';

        
        
        $dataPerhalaman = 10;
        ($hal == '') ? $nohalaman = 1 : $nohalaman = $hal;
        $offset = ($nohalaman - 1) * $dataPerhalaman;
        $off = abs((int) $offset);
        
        $data['datas']  = $this->Equipment_Model->get_equipment_by_depo($_SESSION['depo_user']['depo_id'],$dataPerhalaman, $off, $year, $month);
        $jumlahData =  $this->Equipment_Model->count_equipment_by_depo($_SESSION['depo_user']['depo_id'], $year, $month);
        
        $data['paginator'] = $this->Equipment_Model->page($jumlahData, $dataPerhalaman, $hal);
        
        $data['year'] = $year;
        $data['month'] = $month;
        
        $data['depo']  = $this->Depo_Model->get_single('depo', $_SESSION['depo_user']['depo_id'], 'id');

        $this->load->view('report/depo_v', $data);
    }

    public function equipment()
    {
        $this->lib_auth->check('yes');

        $data['page_title'] = APP_NAME . " | Report Equipment";
        
        $find = $this->input->get("eq_nbr");
        if (!empty($find))
        {
            $data['container'] = $this->Equipment_Model->get_single("equipment_uses", $find, "eq_nbr");
            $data['damage'] = $this->Equipment_Model->get_datas("equipment_damages", $find, "eq_nbr");
            $data['foto'] = $this->Equipment_Model->get_single("equipment_foto", $find, "eq_nbr");
            $data['side'] = array("topside","bottomside","rearside", "frontside", "rightside", "leftside", "inside");
        }
        
        $this->load->view('report/equipment_v', $data);
    }

}
