<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lib_auth {

    public function login()
    {
        $this->load->view('admin/login');
    }

    /* ------------------------------------------------------------------------------------------------------- */

    public function save($data)
    {
        $_SESSION[SESSION_NAME] = $data;
    }

    public function check($redirect = 'yes')
    {
        if (!isset($_SESSION[SESSION_NAME]))
        {

            if ($redirect == 'yes')
            {
                redirect(base_url() . 'auth/');
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return TRUE;
        }
    }

    public function logout()
    {
        unset($_SESSION[SESSION_NAME]);
        redirect(base_url() . 'auth/');
    }
    
    public function get_role(){
        
    }

}