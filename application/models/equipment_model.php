<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Equipment_Model extends MY_Model {

    private $table_primary = "equipment_uses";
    private $table_secondary = "customers";
    private $table_temp = "equipment_temp";
    private $table_foto = "equipment_foto";

    function get_equipment_by_sort($sort, $by)
    {
        $sql = "select * from " . $this->table_primary . " order by " . $sort . " " . $by;

        $query = $this->db->query($sql);
        $data = $query->result_array();
        return $data;
    }

    function get_equipment_by_depo($depo_id, $limit = 10, $offset = 0, $year, $month)
    {
        $sql = "select * from " . $this->table_primary . " where depo_id = '" . $this->db->escape_str($depo_id) . 
                "' and in_time like '" . $year . "-" . $month . "%' limit " . $offset . "," . $limit.";";
        $query = $this->db->query($sql);
        $data = $query->result_array();
        return $data;
    }

    function count_equipment_by_depo($depo_id, $year, $month)
    {
        $sql = "select * from " . $this->table_primary . " where depo_id = '" . $this->db->escape_str($depo_id) . 
                "' and in_time like '" . $year . "-" . $month . "%';";
        $query = $this->db->query($sql);
        $data = $query->num_rows();
        return $data;
    }

    function get_temp($eq_nbr)
    {
        $data = 0;
        $sql = "select * from " . $this->table_temp . " where eq_nbr='" . $eq_nbr . "'";

        $query = $this->db->query($sql);
        $data = $query->num_rows();

        $sql1 = "select * from " . $this->table_foto . " where eq_nbr='" . $eq_nbr . "'";
        $query1 = $this->db->query($sql1);
        $data1 = $query1->num_rows();

        $data = $data + $data1;
        return (int) $data;
    }

}
