<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_Model extends MY_Model {

    private $table = "users";

    function cek_login($username, $password)
    {
        $query = "select * from users where unique_id = '" . $this->db->escape_str($username) . "';";
        $q = $this->db->query($query);
        $data = $q->row();

        if ($data) {
            if ($data->encrypted_password == $password) {
                unset($data->encrypted_password);
                unset($data->salt);
                return (object) array('status' => 1, 'user' => $data);
            } else {
                return (object) array('status' => 0, 'error' => 'Please correct your Password!');
            }
        } else {
            return (object) array('status' => 0, 'error' => 'Please correct yout Account!');
        }
    }

    function check_username($username)
    {
        $query = "select * from users where unique_id = '" . $this->db->escape_str($username) . "';";
        $q = $this->db->query($query);
        $data = $q->row();

        if ($data) {
            return false;
        } else {
            return true;
        }
    }

}
