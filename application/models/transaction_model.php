<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transaction_Model extends MY_Model {

    private $table_primary = "transaction";
    private $table_secondary = "customers";
    private $table_temp = "customers";

    function get_trx_id()
    {
        $sql = "SELECT trx_id FROM `transaction`ORDER BY `transaction`.`trx_id`  DESC limit 0 , 1;";
        $query = $this->db->query($sql);

        $result = $query->row_array();

        $count = $query->num_rows();
        if ($count > 0)
        {
            $back = $result['trx_id'];
        }
        else
        {
            $back = 0;
        }
        return $back;
//        return isset$result['trx_id']?1:$result['trx_id'];
    }

    function count_type_20($trx_id)
    {
        $sql = "select count(*) as total from equipment_uses where trx_id='" . $trx_id . "'and eq_size=20;";
        $query = $this->db->query($sql);

        $result = $query->row_array();
        return $result['total'];
    }
    
    function count_type_40($trx_id)
    {
        $sql = "select count(*) as total from equipment_uses where trx_id='" . $trx_id . "'and eq_size=40;";
        $query = $this->db->query($sql);

        $result = $query->row_array();
        return $result['total'];
    }
    
    function get_transaction_by_sort($sort, $by)
    {
        $sql = "select * from ".$this->table_primary." order by ".$sort." ".$by;
        
        $query = $this->db->query($sql);
        $data = $query->result_array();
        return $data;
    }

}