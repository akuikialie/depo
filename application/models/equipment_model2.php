<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Equipment_Model extends MY_Model {

    private $table_primary = "equipment_uses";
    private $table_secondary = "customers";
    private $table_temp = "customers";

    function get_equipment_by_sort($sort, $by)
    {
        $sql = "select * from ".$this->table_primary." order by ".$sort." ".$by;
        
        $query = $this->db->query($sql);
        $data = $query->result_array();
        return $data;
    }
}