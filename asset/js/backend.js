$(document).ready(function() {
	
	$(".sb-menu-link").click(function(){
		$(this).next().slideToggle('fast');
		$(this).toggleClass('sidebar_menu_open','sidebar_menu_close');
	});
	
	H5F.setup(document.getElementById("f1"));
	$(".fancy_upload").fancybox({
										maxWidth	: 800,
										maxHeight	: 600,
										fitToView	: false,
										width		: '70%',
										height		: '70%',
										autoSize	: false,
										closeClick	: false,
										openEffect	: 'none',
										closeEffect	: 'none'
									});		
	
});