<?php 
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");
include("include/dbcommon.php");
include("classes/searchclause.php");
session_cache_limiter("none");

include("include/equipment_uses_variables.php");

if(!@$_SESSION["UserID"])
{ 
	$_SESSION["MyURL"]=$_SERVER["SCRIPT_NAME"]."?".$_SERVER["QUERY_STRING"];
	header("Location: login.php?message=expired"); 
	return;
}
if(!CheckSecurity(@$_SESSION["_".$strTableName."_OwnerID"],"Export"))
{
	echo "<p>"."You don't have permissions to access this table"."<a href=\"login.php\">"."Back to login page"."</a></p>";
	return;
}

$layout = new TLayout("export","BoldOrange","MobileOrange");
$layout->blocks["top"] = array();
$layout->containers["export"] = array();

$layout->containers["export"][] = array("name"=>"exportheader","block"=>"","substyle"=>2);


$layout->containers["export"][] = array("name"=>"exprange_header","block"=>"rangeheader_block","substyle"=>3);


$layout->containers["export"][] = array("name"=>"exprange","block"=>"range_block","substyle"=>1);


$layout->containers["export"][] = array("name"=>"expoutput_header","block"=>"","substyle"=>3);


$layout->containers["export"][] = array("name"=>"expoutput","block"=>"","substyle"=>1);


$layout->containers["export"][] = array("name"=>"expbuttons","block"=>"","substyle"=>2);


$layout->skins["export"] = "fields";
$layout->blocks["top"][] = "export";$page_layouts["equipment_uses_export"] = $layout;


// Modify query: remove blob fields from fieldlist.
// Blob fields on an export page are shown using imager.php (for example).
// They don't need to be selected from DB in export.php itself.
//$gQuery->ReplaceFieldsWithDummies(GetBinaryFieldsIndices());

//	Before Process event
if($eventObj->exists("BeforeProcessExport"))
	$eventObj->BeforeProcessExport($conn);

$strWhereClause="";
$strHavingClause="";
$selected_recs=array();
$options = "1";

header("Expires: Thu, 01 Jan 1970 00:00:01 GMT"); 
include('include/xtempl.php');
include('classes/runnerpage.php');
$xt = new Xtempl();
include("include/export_functions.php");
$id = postvalue("id") != "" ? postvalue("id") : 1;
//array of params for classes
$params = array("pageType" => PAGE_EXPORT, "id" =>$id, "tName"=>$strTableName);
$params["xt"] = &$xt;
if(!$eventObj->exists("ListGetRowCount") && !$eventObj->exists("ListQuery"))
	$params["needSearchClauseObj"] = false;
$pageObject = new RunnerPage($params);

if (@$_REQUEST["a"]!="")
{
	$options = "";
	$sWhere = "1=0";	

//	process selection
	$selected_recs=array();
	if (@$_REQUEST["mdelete"])
	{
		foreach(@$_REQUEST["mdelete"] as $ind)
		{
			$keys=array();
			$keys["gkey"]=refine($_REQUEST["mdelete1"][mdeleteIndex($ind)]);
			$selected_recs[]=$keys;
		}
	}
	elseif(@$_REQUEST["selection"])
	{
		foreach(@$_REQUEST["selection"] as $keyblock)
		{
			$arr=explode("&",refine($keyblock));
			if(count($arr)<1)
				continue;
			$keys=array();
			$keys["gkey"]=urldecode($arr[0]);
			$selected_recs[]=$keys;
		}
	}

	foreach($selected_recs as $keys)
	{
		$sWhere = $sWhere . " or ";
		$sWhere.=KeyWhere($keys);
	}


	$strSQL = gSQLWhere($sWhere);
	$strWhereClause=$sWhere;
	
	$_SESSION[$strTableName."_SelectedSQL"] = $strSQL;
	$_SESSION[$strTableName."_SelectedWhere"] = $sWhere;
	$_SESSION[$strTableName."_SelectedRecords"] = $selected_recs;
}

if ($_SESSION[$strTableName."_SelectedSQL"]!="" && @$_REQUEST["records"]=="") 
{
	$strSQL = $_SESSION[$strTableName."_SelectedSQL"];
	$strWhereClause=@$_SESSION[$strTableName."_SelectedWhere"];
	$selected_recs = $_SESSION[$strTableName."_SelectedRecords"];
}
else
{
	$strWhereClause=@$_SESSION[$strTableName."_where"];
	$strHavingClause=@$_SESSION[$strTableName."_having"];
	$strSQL=gSQLWhere($strWhereClause,$strHavingClause);
}

$mypage=1;
if(@$_REQUEST["type"])
{
//	order by
	$strOrderBy=$_SESSION[$strTableName."_order"];
	if(!$strOrderBy)
		$strOrderBy=$gstrOrderBy;
	$strSQL.=" ".trim($strOrderBy);

	$strSQLbak = $strSQL;
	if($eventObj->exists("BeforeQueryExport"))
		$eventObj->BeforeQueryExport($strSQL,$strWhereClause,$strOrderBy);
//	Rebuild SQL if needed
	if($strSQL!=$strSQLbak)
	{
//	changed $strSQL - old style	
		$numrows=GetRowCount($strSQL);
	}
	else
	{
		$strSQL = gSQLWhere($strWhereClause,$strHavingClause);
		$strSQL.=" ".trim($strOrderBy);
		$rowcount=false;
		if($eventObj->exists("ListGetRowCount"))
		{
			$masterKeysReq=array();
			for($i = 0; $i < count($pageObject->detailKeysByM); $i ++)
				$masterKeysReq[]=$_SESSION[$strTableName."_masterkey".($i + 1)];
			$rowcount=$eventObj->ListGetRowCount($pageObject->searchClauseObj,$_SESSION[$strTableName."_mastertable"],$masterKeysReq,$selected_recs);
		}
		if($rowcount!==false)
			$numrows=$rowcount;
		else
			$numrows=gSQLRowCount($strWhereClause,$strHavingClause);
	}
	LogInfo($strSQL);

//	 Pagination:

	$nPageSize = 0;
	if(@$_REQUEST["records"]=="page" && $numrows)
	{
		$mypage = (integer)@$_SESSION[$strTableName."_pagenumber"];
		$nPageSize = (integer)@$_SESSION[$strTableName."_pagesize"];
		
		if(!$nPageSize)
			$nPageSize = GetTableData($strTableName,".pageSize",0);
				
		if($nPageSize<0)
			$nPageSize = 0;
			
		if($nPageSize>0)
		{
			if($numrows<=($mypage-1)*$nPageSize)
				$mypage = ceil($numrows/$nPageSize);
		
			if(!$mypage)
				$mypage = 1;
			
					$strSQL.=" limit ".(($mypage-1)*$nPageSize).",".$nPageSize;
		}
	}
	$listarray = false;
	if($eventObj->exists("ListQuery"))
		$listarray = $eventObj->ListQuery($pageObject->searchClauseObj,$_SESSION[$strTableName."_arrFieldForSort"],$_SESSION[$strTableName."_arrHowFieldSort"],$_SESSION[$strTableName."_mastertable"],$masterKeysReq,$selected_recs,$nPageSize,$mypage);
	if($listarray!==false)
		$rs = $listarray;
	elseif($nPageSize>0)
	{
					$rs = db_query($strSQL,$conn);
	}
	else
		$rs = db_query($strSQL,$conn);

	if(!ini_get("safe_mode"))
		set_time_limit(300);
	
	if(substr(@$_REQUEST["type"],0,5)=="excel")
	{
//	remove grouping
		$locale_info["LOCALE_SGROUPING"]="0";
		$locale_info["LOCALE_SMONGROUPING"]="0";
		ExportToExcel();
	}
	else if(@$_REQUEST["type"]=="word")
	{
		ExportToWord();
	}
	else if(@$_REQUEST["type"]=="xml")
	{
		ExportToXML();
	}
	else if(@$_REQUEST["type"]=="csv")
	{
		$locale_info["LOCALE_SGROUPING"]="0";
		$locale_info["LOCALE_SDECIMAL"]=".";
		$locale_info["LOCALE_SMONGROUPING"]="0";
		$locale_info["LOCALE_SMONDECIMALSEP"]=".";
		ExportToCSV();
	}
	db_close($conn);
	return;
}

// add button events if exist
$pageObject->addButtonHandlers();

if($options)
{
	$xt->assign("rangeheader_block",true);
	$xt->assign("range_block",true);
}

$xt->assign("exportlink_attrs", 'id="saveButton'.$pageObject->id.'"');

$pageObject->body["begin"] .="<script type=\"text/javascript\" src=\"include/loadfirst.js\"></script>\r\n";
$pageObject->body["begin"] .= "<script type=\"text/javascript\" src=\"include/lang/".getLangFileName(mlang_getcurrentlang()).".js\"></script>";

$pageObject->fillSetCntrlMaps();
$pageObject->body['end'] .= '<script>';
$pageObject->body['end'] .= "window.controlsMap = ".my_json_encode($pageObject->controlsHTMLMap).";";
$pageObject->body['end'] .= "window.settings = ".my_json_encode($pageObject->jsSettings).";";
$pageObject->body['end'] .= '</script>';
$pageObject->body["end"] .= "<script language=\"JavaScript\" src=\"include/runnerJS/RunnerAll.js\"></script>\r\n";
$pageObject->addCommonJs();

$pageObject->body["end"] .= "<script>".$pageObject->PrepareJS()."</script>";
$xt->assignbyref("body",$pageObject->body);

$xt->display("equipment_uses_export.htm");

function ExportToWord()
{
	global $cCharset;
	header("Content-Type: application/vnd.ms-word");
	header("Content-Disposition: attachment;Filename=equipment_uses.doc");

	echo "<html>";
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=".$cCharset."\">";
	echo "<body>";
	echo "<table border=1>";

	WriteTableData();

	echo "</table>";
	echo "</body>";
	echo "</html>";
}

function ExportToXML()
{
	global $nPageSize,$rs,$strTableName,$conn,$eventObj;
	header("Content-Type: text/xml");
	header("Content-Disposition: attachment;Filename=equipment_uses.xml");
	if($eventObj->exists("ListFetchArray"))
		$row = $eventObj->ListFetchArray($rs);
	else
		$row = db_fetch_array($rs);	
	//if(!$row)
	//	return;
		
	global $cCharset;
	
	echo "<?xml version=\"1.0\" encoding=\"".$cCharset."\" standalone=\"yes\"?>\r\n";
	echo "<table>\r\n";
	$i=0;
	
	
	while((!$nPageSize || $i<$nPageSize) && $row)
	{
		
		$values = array();
			$values["gkey"] = GetData($row,"gkey","");
			$values["eq_nbr"] = GetData($row,"eq_nbr","");
			$values["line_id"] = GetData($row,"line_id","");
			$values["depo_id"] = GetData($row,"depo_id","");
			$values["shipping_agents_id"] = GetData($row,"shipping_agents_id","");
			$values["shipping_agent_name"] = GetData($row,"shipping_agent_name","");
			$values["creator"] = GetData($row,"creator","");
			$values["created"] = GetData($row,"created","");
			$values["port_of_origin"] = GetData($row,"port_of_origin","");
			$values["consignee"] = GetData($row,"consignee","");
			$values["port_of_destination"] = GetData($row,"port_of_destination","");
			$values["bl_nbr"] = GetData($row,"bl_nbr","");
			$values["category"] = GetData($row,"category","");
			$values["document_verfied"] = GetData($row,"document_verfied","");
			$values["survey_pos_id"] = GetData($row,"survey_pos_id","");
			$values["ctr_position"] = GetData($row,"ctr_position","");
			$values["iso_code"] = GetData($row,"iso_code","");
			$values["eq_size"] = GetData($row,"eq_size","");
			$values["eq_type"] = GetData($row,"eq_type","");
			$values["shipping_line_name"] = GetData($row,"shipping_line_name","");
			$values["tare_weight_unit"] = GetData($row,"tare_weight_unit","");
			$values["payment_release"] = GetData($row,"payment_release","");
			$values["trx_id"] = GetData($row,"trx_id","");
		
		
		$eventRes = true;
		if ($eventObj->exists('BeforeOut'))
		{			
			$eventRes = $eventObj->BeforeOut($row, $values);
		}
		if ($eventRes)
		{
			$i++;
			echo "<row>\r\n";
			foreach ($values as $fName => $val)
			{
				$field = htmlspecialchars(XMLNameEncode($fName));
				echo "<".$field.">";
				echo htmlspecialchars($values[$fName]);
				echo "</".$field.">\r\n";
			}
			echo "</row>\r\n";
		}
		
		
		if($eventObj->exists("ListFetchArray"))
			$row = $eventObj->ListFetchArray($rs);
		else
			$row = db_fetch_array($rs);	
	}
	echo "</table>\r\n";
}

function ExportToCSV()
{
	global $rs,$nPageSize,$strTableName,$conn,$eventObj;
	header("Content-Type: application/csv");
	header("Content-Disposition: attachment;Filename=equipment_uses.csv");
	
	if($eventObj->exists("ListFetchArray"))
		$row = $eventObj->ListFetchArray($rs);
	else
		$row = db_fetch_array($rs);	
//	if(!$row)
//		return;
	
		
		
	$totals=array();
	$totals["eq_nbr"]=0;

	
// write header
	$outstr="";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"gkey\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"eq_nbr\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"line_id\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"depo_id\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"shipping_agents_id\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"shipping_agent_name\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"creator\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"created\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"port_of_origin\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"consignee\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"port_of_destination\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"bl_nbr\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"category\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"document_verfied\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"survey_pos_id\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"ctr_position\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"iso_code\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"eq_size\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"eq_type\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"shipping_line_name\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"tare_weight_unit\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"payment_release\"";
	if($outstr!="")
		$outstr.=",";
	$outstr.= "\"trx_id\"";
	echo $outstr;
	echo "\r\n";

// write data rows
	$iNumberOfRows = 0;
	while((!$nPageSize || $iNumberOfRows<$nPageSize) && $row)
	{
		
		
			$totals["eq_nbr"]+= ($row["eq_nbr"]!="");
		$values = array();
			$format="";
			$values["gkey"] = GetData($row,"gkey",$format);
			$format="";
			$values["eq_nbr"] = GetData($row,"eq_nbr",$format);
			$format="";
			$values["line_id"] = GetData($row,"line_id",$format);
			$format="";
			$values["depo_id"] = GetData($row,"depo_id",$format);
			$format="";
			$values["shipping_agents_id"] = GetData($row,"shipping_agents_id",$format);
			$format="";
			$values["shipping_agent_name"] = GetData($row,"shipping_agent_name",$format);
			$format="";
			$values["creator"] = GetData($row,"creator",$format);
			$format="Short Date";
			$values["created"] = GetData($row,"created",$format);
			$format="";
			$values["port_of_origin"] = GetData($row,"port_of_origin",$format);
			$format="";
			$values["consignee"] = GetData($row,"consignee",$format);
			$format="";
			$values["port_of_destination"] = GetData($row,"port_of_destination",$format);
			$format="";
			$values["bl_nbr"] = GetData($row,"bl_nbr",$format);
			$format="";
			$values["category"] = GetData($row,"category",$format);
			$format="";
			$values["document_verfied"] = GetData($row,"document_verfied",$format);
			$format="";
			$values["survey_pos_id"] = GetData($row,"survey_pos_id",$format);
			$format="";
			$values["ctr_position"] = GetData($row,"ctr_position",$format);
			$format="";
			$values["iso_code"] = GetData($row,"iso_code",$format);
			$format="";
			$values["eq_size"] = GetData($row,"eq_size",$format);
			$format="";
			$values["eq_type"] = GetData($row,"eq_type",$format);
			$format="";
			$values["shipping_line_name"] = GetData($row,"shipping_line_name",$format);
			$format="";
			$values["tare_weight_unit"] = GetData($row,"tare_weight_unit",$format);
			$format="";
			$values["payment_release"] = GetData($row,"payment_release",$format);
			$format="";
			$values["trx_id"] = GetData($row,"trx_id",$format);

		$eventRes = true;
		if ($eventObj->exists('BeforeOut'))
		{			
			$eventRes = $eventObj->BeforeOut($row,$values);
		}
		if ($eventRes)
		{
			$outstr="";			
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["gkey"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["eq_nbr"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["line_id"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["depo_id"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["shipping_agents_id"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["shipping_agent_name"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["creator"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["created"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["port_of_origin"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["consignee"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["port_of_destination"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["bl_nbr"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["category"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["document_verfied"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["survey_pos_id"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["ctr_position"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["iso_code"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["eq_size"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["eq_type"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["shipping_line_name"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["tare_weight_unit"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["payment_release"]).'"';
			if($outstr!="")
				$outstr.=",";
			$outstr.='"'.str_replace('"','""',$values["trx_id"]).'"';
			echo $outstr;
		}
		
		$iNumberOfRows++;
		if($eventObj->exists("ListFetchArray"))
			$row = $eventObj->ListFetchArray($rs);
		else
			$row = db_fetch_array($rs);	
			
		if(((!$nPageSize || $iNumberOfRows<$nPageSize) && $row) && $eventRes)
			echo "\r\n";
	}
}


function WriteTableData()
{
	global $rs,$nPageSize,$strTableName,$conn,$eventObj;
	
	if($eventObj->exists("ListFetchArray"))
		$row = $eventObj->ListFetchArray($rs);
	else
		$row = db_fetch_array($rs);	
//	if(!$row)
//		return;
// write header
	echo "<tr>";
	if($_REQUEST["type"]=="excel")
	{
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Gkey").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Eq Nbr").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Line Id").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Depo Id").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Shipping Agents Id").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Shipping Agent Name").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Creator").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Created").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Port Of Origin").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Consignee").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Port Of Destination").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Bl Nbr").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Category").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Document Verfied").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Survey Pos Id").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Ctr Position").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Iso Code").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Eq Size").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Eq Type").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Shipping Line Name").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Tare Weight Unit").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Payment Release").'</td>';	
		echo '<td style="width: 100" x:str>'.PrepareForExcel("Trx Id").'</td>';	
	}
	else
	{
		echo "<td>"."Gkey"."</td>";
		echo "<td>"."Eq Nbr"."</td>";
		echo "<td>"."Line Id"."</td>";
		echo "<td>"."Depo Id"."</td>";
		echo "<td>"."Shipping Agents Id"."</td>";
		echo "<td>"."Shipping Agent Name"."</td>";
		echo "<td>"."Creator"."</td>";
		echo "<td>"."Created"."</td>";
		echo "<td>"."Port Of Origin"."</td>";
		echo "<td>"."Consignee"."</td>";
		echo "<td>"."Port Of Destination"."</td>";
		echo "<td>"."Bl Nbr"."</td>";
		echo "<td>"."Category"."</td>";
		echo "<td>"."Document Verfied"."</td>";
		echo "<td>"."Survey Pos Id"."</td>";
		echo "<td>"."Ctr Position"."</td>";
		echo "<td>"."Iso Code"."</td>";
		echo "<td>"."Eq Size"."</td>";
		echo "<td>"."Eq Type"."</td>";
		echo "<td>"."Shipping Line Name"."</td>";
		echo "<td>"."Tare Weight Unit"."</td>";
		echo "<td>"."Payment Release"."</td>";
		echo "<td>"."Trx Id"."</td>";
	}
	echo "</tr>";
		$totals["gkey"]=0;
		$totalsFields[]=array('fName'=>"gkey", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["eq_nbr"]=0;
		$totalsFields[]=array('fName'=>"eq_nbr", 'totalsType'=>'COUNT', 'viewFormat'=>"");
		$totals["line_id"]=0;
		$totalsFields[]=array('fName'=>"line_id", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["depo_id"]=0;
		$totalsFields[]=array('fName'=>"depo_id", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["shipping_agents_id"]=0;
		$totalsFields[]=array('fName'=>"shipping_agents_id", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["shipping_agent_name"]=0;
		$totalsFields[]=array('fName'=>"shipping_agent_name", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["creator"]=0;
		$totalsFields[]=array('fName'=>"creator", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["created"]=0;
		$totalsFields[]=array('fName'=>"created", 'totalsType'=>'', 'viewFormat'=>"Short Date");
		$totals["port_of_origin"]=0;
		$totalsFields[]=array('fName'=>"port_of_origin", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["consignee"]=0;
		$totalsFields[]=array('fName'=>"consignee", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["port_of_destination"]=0;
		$totalsFields[]=array('fName'=>"port_of_destination", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["bl_nbr"]=0;
		$totalsFields[]=array('fName'=>"bl_nbr", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["category"]=0;
		$totalsFields[]=array('fName'=>"category", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["document_verfied"]=0;
		$totalsFields[]=array('fName'=>"document_verfied", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["survey_pos_id"]=0;
		$totalsFields[]=array('fName'=>"survey_pos_id", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["ctr_position"]=0;
		$totalsFields[]=array('fName'=>"ctr_position", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["iso_code"]=0;
		$totalsFields[]=array('fName'=>"iso_code", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["eq_size"]=0;
		$totalsFields[]=array('fName'=>"eq_size", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["eq_type"]=0;
		$totalsFields[]=array('fName'=>"eq_type", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["shipping_line_name"]=0;
		$totalsFields[]=array('fName'=>"shipping_line_name", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["tare_weight_unit"]=0;
		$totalsFields[]=array('fName'=>"tare_weight_unit", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["payment_release"]=0;
		$totalsFields[]=array('fName'=>"payment_release", 'totalsType'=>'', 'viewFormat'=>"");
		$totals["trx_id"]=0;
		$totalsFields[]=array('fName'=>"trx_id", 'totalsType'=>'', 'viewFormat'=>"");
	$totals=array();
	$totals["eq_nbr"]=0;
// write data rows
	$iNumberOfRows = 0;
	while((!$nPageSize || $iNumberOfRows<$nPageSize) && $row)
	{
		countTotals($totals,$totalsFields, $row);
			
		$values = array();	

					
							$format="";
			
			$values["gkey"] = GetData($row,"gkey",$format);
					
							$format="";
			
			$values["eq_nbr"] = GetData($row,"eq_nbr",$format);
					
							$format="";
			
			$values["line_id"] = GetData($row,"line_id",$format);
					
							$format="";
			
			$values["depo_id"] = GetData($row,"depo_id",$format);
					
							$format="";
			
			$values["shipping_agents_id"] = GetData($row,"shipping_agents_id",$format);
					
							$format="";
			
			$values["shipping_agent_name"] = GetData($row,"shipping_agent_name",$format);
					
							$format="";
			
			$values["creator"] = GetData($row,"creator",$format);
					
							$format="Short Date";
			
			$values["created"] = GetData($row,"created",$format);
					
							$format="";
			
			$values["port_of_origin"] = GetData($row,"port_of_origin",$format);
					
							$format="";
			
			$values["consignee"] = GetData($row,"consignee",$format);
					
							$format="";
			
			$values["port_of_destination"] = GetData($row,"port_of_destination",$format);
					
							$format="";
			
			$values["bl_nbr"] = GetData($row,"bl_nbr",$format);
					
							$format="";
			
			$values["category"] = GetData($row,"category",$format);
					
							$format="";
			
			$values["document_verfied"] = GetData($row,"document_verfied",$format);
					
							$format="";
			
			$values["survey_pos_id"] = GetData($row,"survey_pos_id",$format);
					
							$format="";
			
			$values["ctr_position"] = GetData($row,"ctr_position",$format);
					
							$format="";
			
			$values["iso_code"] = GetData($row,"iso_code",$format);
					
							$format="";
			
			$values["eq_size"] = GetData($row,"eq_size",$format);
					
							$format="";
			
			$values["eq_type"] = GetData($row,"eq_type",$format);
					
							$format="";
			
			$values["shipping_line_name"] = GetData($row,"shipping_line_name",$format);
					
							$format="";
			
			$values["tare_weight_unit"] = GetData($row,"tare_weight_unit",$format);
					
							$format="";
			
			$values["payment_release"] = GetData($row,"payment_release",$format);
					
							$format="";
			
			$values["trx_id"] = GetData($row,"trx_id",$format);

		
		$eventRes = true;
		if ($eventObj->exists('BeforeOut'))
		{			
			$eventRes = $eventObj->BeforeOut($row, $values);
		}
		if ($eventRes)
		{
			$iNumberOfRows++;
			echo "<tr>";

							echo '<td>';
						
			
									$format="";
									echo htmlspecialchars($values["gkey"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["eq_nbr"]);
					else
						echo htmlspecialchars($values["eq_nbr"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["line_id"]);
					else
						echo htmlspecialchars($values["line_id"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["depo_id"]);
					else
						echo htmlspecialchars($values["depo_id"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["shipping_agents_id"]);
					else
						echo htmlspecialchars($values["shipping_agents_id"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["shipping_agent_name"]);
					else
						echo htmlspecialchars($values["shipping_agent_name"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["creator"]);
					else
						echo htmlspecialchars($values["creator"]);
			echo '</td>';
							echo '<td>';
						
			
									$format="Short Date";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["created"]);
					else
						echo htmlspecialchars($values["created"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["port_of_origin"]);
					else
						echo htmlspecialchars($values["port_of_origin"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["consignee"]);
					else
						echo htmlspecialchars($values["consignee"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["port_of_destination"]);
					else
						echo htmlspecialchars($values["port_of_destination"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["bl_nbr"]);
					else
						echo htmlspecialchars($values["bl_nbr"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["category"]);
					else
						echo htmlspecialchars($values["category"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["document_verfied"]);
					else
						echo htmlspecialchars($values["document_verfied"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["survey_pos_id"]);
					else
						echo htmlspecialchars($values["survey_pos_id"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["ctr_position"]);
					else
						echo htmlspecialchars($values["ctr_position"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["iso_code"]);
					else
						echo htmlspecialchars($values["iso_code"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["eq_size"]);
					else
						echo htmlspecialchars($values["eq_size"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["eq_type"]);
					else
						echo htmlspecialchars($values["eq_type"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["shipping_line_name"]);
					else
						echo htmlspecialchars($values["shipping_line_name"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["tare_weight_unit"]);
					else
						echo htmlspecialchars($values["tare_weight_unit"]);
			echo '</td>';
							if($_REQUEST["type"]=="excel")
					echo '<td x:str>';
				else
					echo '<td>';
						
			
									$format="";
									if($_REQUEST["type"]=="excel")
						echo PrepareForExcel($values["payment_release"]);
					else
						echo htmlspecialchars($values["payment_release"]);
			echo '</td>';
							echo '<td>';
						
			
									$format="";
									echo htmlspecialchars($values["trx_id"]);
			echo '</td>';
			echo "</tr>";
		}		
		
		
		if($eventObj->exists("ListFetchArray"))
			$row = $eventObj->ListFetchArray($rs);
		else
			$row = db_fetch_array($rs);	
	}
	
	echo "<tr>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
						echo "Count".": ";
	echo htmlspecialchars(GetTotals("eq_nbr", $totals["eq_nbr"], "COUNT", $iNumberOfRows, ""));
	echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "<td>";
		echo "</td>";
	echo "</tr>";
}

function XMLNameEncode($strValue)
{	
	$search=array(" ","#","'","/","\\","(",")",",","[");
	$ret=str_replace($search,"",$strValue);
	$search=array("]","+","\"","-","_","|","}","{","=");
	$ret=str_replace($search,"",$ret);
	return $ret;
}

function PrepareForExcel($str)
{
	$ret = htmlspecialchars($str);
	if (substr($ret,0,1)== "=") 
		$ret = "&#61;".substr($ret,1);
	return $ret;

}

function countTotals(&$totals,$totalsFields, $data) 
{
	for($i = 0; $i < count($totalsFields); $i ++) 
	{
		if($totalsFields[$i]['totalsType'] == 'COUNT') 
			$totals[$totalsFields[$i]['fName']]+=($data[$totalsFields[$i]['fName']]!= "");
		else if($totalsFields[$i]['viewFormat'] == "Time") 
		{
			$time = GetTotalsForTime($data[$totalsFields[$i]['fName']]);
			$totals[$totalsFields[$i]['fName']] += $time[2]+$time[1]*60 + $time[0]*3600;
		} 
		else 
			$totals[$totalsFields[$i]['fName']]+=($data[$totalsFields[$i]['fName']]+ 0);
	}
}
?>
