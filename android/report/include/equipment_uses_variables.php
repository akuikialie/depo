<?php
$strTableName="equipment_uses";
$_SESSION["OwnerID"] = $_SESSION["_".$strTableName."_OwnerID"];

$strOriginalTableName="equipment_uses";

$gstrOrderBy="";
if(strlen($gstrOrderBy) && strtolower(substr($gstrOrderBy,0,8))!="order by")
	$gstrOrderBy="order by ".$gstrOrderBy;

$g_orderindexes=array();
$gsqlHead="SELECT gkey,  eq_nbr,  line_id,  depo_id,  shipping_agents_id,  shipping_agent_name,  creator,  created,  port_of_origin,  consignee,  port_of_destination,  bl_nbr,  category,  document_verfied,  survey_pos_id,  ctr_position,  iso_code,  eq_size,  eq_type,  shipping_line_name,  tare_weight_unit,  payment_release,  trx_id";
$gsqlFrom="FROM equipment_uses";
$gsqlWhereExpr="";
$gsqlTail="";

include_once(getabspath("include/equipment_uses_settings.php"));

// alias for 'SQLQuery' object
$gQuery = &$queryData_equipment_uses;
$eventObj = &$tableEvents["equipment_uses"];

$reportCaseSensitiveGroupFields = false;

$gstrSQL = gSQLWhere("");


?>