<?php
$dalTabletransaction = array();
$dalTabletransaction["trx_id"] = array("type"=>3,"varname"=>"trx_id");
$dalTabletransaction["customer_tax_id"] = array("type"=>200,"varname"=>"customer_tax_id");
$dalTabletransaction["customer_name"] = array("type"=>200,"varname"=>"customer_name");
$dalTabletransaction["vessel_id"] = array("type"=>200,"varname"=>"vessel_id");
$dalTabletransaction["vessel_voyage_id"] = array("type"=>200,"varname"=>"vessel_voyage_id");
$dalTabletransaction["vessel_name"] = array("type"=>200,"varname"=>"vessel_name");
$dalTabletransaction["doc_number"] = array("type"=>200,"varname"=>"doc_number");
$dalTabletransaction["trx_type_id"] = array("type"=>200,"varname"=>"trx_type_id");
$dalTabletransaction["trx_type_name"] = array("type"=>200,"varname"=>"trx_type_name");
$dalTabletransaction["created"] = array("type"=>135,"varname"=>"created");
$dalTabletransaction["creator"] = array("type"=>200,"varname"=>"creator");
$dalTabletransaction["changed"] = array("type"=>135,"varname"=>"changed");
$dalTabletransaction["changer"] = array("type"=>200,"varname"=>"changer");
$dalTabletransaction["start_timestamp"] = array("type"=>135,"varname"=>"start_timestamp");
$dalTabletransaction["end_timestamp"] = array("type"=>135,"varname"=>"end_timestamp");
$dalTabletransaction["bat_nbr"] = array("type"=>200,"varname"=>"bat_nbr");
$dalTabletransaction["shipping_agent_id"] = array("type"=>200,"varname"=>"shipping_agent_id");
$dalTabletransaction["shipping_agent_name"] = array("type"=>200,"varname"=>"shipping_agent_name");
$dalTabletransaction["trucking_company_id"] = array("type"=>200,"varname"=>"trucking_company_id");
$dalTabletransaction["trucking_company_name"] = array("type"=>200,"varname"=>"trucking_company_name");
$dalTabletransaction["depo_release"] = array("type"=>200,"varname"=>"depo_release");
$dalTabletransaction["payment_release"] = array("type"=>200,"varname"=>"payment_release");
$dalTabletransaction["depo_id"] = array("type"=>200,"varname"=>"depo_id");
$dalTabletransaction["truck_license_nbr"] = array("type"=>200,"varname"=>"truck_license_nbr");
	$dalTabletransaction["trx_id"]["key"]=true;
$dal_info["transaction"]=&$dalTabletransaction;

?>