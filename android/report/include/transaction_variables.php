<?php
$strTableName="transaction";
$_SESSION["OwnerID"] = $_SESSION["_".$strTableName."_OwnerID"];

$strOriginalTableName="transaction";

$gstrOrderBy="";
if(strlen($gstrOrderBy) && strtolower(substr($gstrOrderBy,0,8))!="order by")
	$gstrOrderBy="order by ".$gstrOrderBy;

$g_orderindexes=array();
$gsqlHead="SELECT trx_id,  depo_id,  trx_type_id,  doc_number,  vessel_name,  vessel_voyage_id,  vessel_id,  customer_name,  shipping_agent_id,  shipping_agent_name";
$gsqlFrom="FROM `transaction`";
$gsqlWhereExpr="";
$gsqlTail="";

include_once(getabspath("include/transaction_settings.php"));

// alias for 'SQLQuery' object
$gQuery = &$queryData_transaction;
$eventObj = &$tableEvents["transaction"];

$reportCaseSensitiveGroupFields = false;

$gstrSQL = gSQLWhere("");


?>