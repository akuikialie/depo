<?php
$tdataequipment_uses=array();
	$tdataequipment_uses[".NumberOfChars"]=80; 
	$tdataequipment_uses[".ShortName"]="equipment_uses";
	$tdataequipment_uses[".OwnerID"]="";
	$tdataequipment_uses[".OriginalTable"]="equipment_uses";


	
//	field labels
$fieldLabelsequipment_uses = array();
if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsequipment_uses["English"]=array();
	$fieldToolTipsequipment_uses["English"]=array();
	$fieldLabelsequipment_uses["English"]["gkey"] = "Gkey";
	$fieldToolTipsequipment_uses["English"]["gkey"] = "";
	$fieldLabelsequipment_uses["English"]["eq_nbr"] = "Eq Nbr";
	$fieldToolTipsequipment_uses["English"]["eq_nbr"] = "";
	$fieldLabelsequipment_uses["English"]["line_id"] = "Line Id";
	$fieldToolTipsequipment_uses["English"]["line_id"] = "";
	$fieldLabelsequipment_uses["English"]["depo_id"] = "Depo Id";
	$fieldToolTipsequipment_uses["English"]["depo_id"] = "";
	$fieldLabelsequipment_uses["English"]["shipping_agents_id"] = "Shipping Agents Id";
	$fieldToolTipsequipment_uses["English"]["shipping_agents_id"] = "";
	$fieldLabelsequipment_uses["English"]["shipping_agent_name"] = "Shipping Agent Name";
	$fieldToolTipsequipment_uses["English"]["shipping_agent_name"] = "";
	$fieldLabelsequipment_uses["English"]["creator"] = "Creator";
	$fieldToolTipsequipment_uses["English"]["creator"] = "";
	$fieldLabelsequipment_uses["English"]["created"] = "Created";
	$fieldToolTipsequipment_uses["English"]["created"] = "";
	$fieldLabelsequipment_uses["English"]["port_of_origin"] = "Port Of Origin";
	$fieldToolTipsequipment_uses["English"]["port_of_origin"] = "";
	$fieldLabelsequipment_uses["English"]["consignee"] = "Consignee";
	$fieldToolTipsequipment_uses["English"]["consignee"] = "";
	$fieldLabelsequipment_uses["English"]["port_of_destination"] = "Port Of Destination";
	$fieldToolTipsequipment_uses["English"]["port_of_destination"] = "";
	$fieldLabelsequipment_uses["English"]["bl_nbr"] = "Bl Nbr";
	$fieldToolTipsequipment_uses["English"]["bl_nbr"] = "";
	$fieldLabelsequipment_uses["English"]["category"] = "Category";
	$fieldToolTipsequipment_uses["English"]["category"] = "";
	$fieldLabelsequipment_uses["English"]["document_verfied"] = "Document Verfied";
	$fieldToolTipsequipment_uses["English"]["document_verfied"] = "";
	$fieldLabelsequipment_uses["English"]["survey_pos_id"] = "Survey Pos Id";
	$fieldToolTipsequipment_uses["English"]["survey_pos_id"] = "";
	$fieldLabelsequipment_uses["English"]["ctr_position"] = "Ctr Position";
	$fieldToolTipsequipment_uses["English"]["ctr_position"] = "";
	$fieldLabelsequipment_uses["English"]["iso_code"] = "Iso Code";
	$fieldToolTipsequipment_uses["English"]["iso_code"] = "";
	$fieldLabelsequipment_uses["English"]["eq_size"] = "Eq Size";
	$fieldToolTipsequipment_uses["English"]["eq_size"] = "";
	$fieldLabelsequipment_uses["English"]["eq_type"] = "Eq Type";
	$fieldToolTipsequipment_uses["English"]["eq_type"] = "";
	$fieldLabelsequipment_uses["English"]["shipping_line_name"] = "Shipping Line Name";
	$fieldToolTipsequipment_uses["English"]["shipping_line_name"] = "";
	$fieldLabelsequipment_uses["English"]["tare_weight_unit"] = "Tare Weight Unit";
	$fieldToolTipsequipment_uses["English"]["tare_weight_unit"] = "";
	$fieldLabelsequipment_uses["English"]["payment_release"] = "Payment Release";
	$fieldToolTipsequipment_uses["English"]["payment_release"] = "";
	$fieldLabelsequipment_uses["English"]["trx_id"] = "Trx Id";
	$fieldToolTipsequipment_uses["English"]["trx_id"] = "";
	if (count($fieldToolTipsequipment_uses["English"])){
		$tdataequipment_uses[".isUseToolTips"]=true;
	}
}


	
	$tdataequipment_uses[".NCSearch"]=true;

	

$tdataequipment_uses[".shortTableName"] = "equipment_uses";
$tdataequipment_uses[".nSecOptions"] = 0;
$tdataequipment_uses[".recsPerRowList"] = 1;	
$tdataequipment_uses[".tableGroupBy"] = "0";
$tdataequipment_uses[".mainTableOwnerID"] = "";
$tdataequipment_uses[".moveNext"] = 1;




$tdataequipment_uses[".showAddInPopup"] = false;

$tdataequipment_uses[".showEditInPopup"] = false;

$tdataequipment_uses[".showViewInPopup"] = false;


$tdataequipment_uses[".fieldsForRegister"] = array();

$tdataequipment_uses[".listAjax"] = false;

	$tdataequipment_uses[".audit"] = false;

	$tdataequipment_uses[".locking"] = false;
	
$tdataequipment_uses[".listIcons"] = true;

$tdataequipment_uses[".exportTo"] = true;

$tdataequipment_uses[".printFriendly"] = true;


$tdataequipment_uses[".showSimpleSearchOptions"] = false;

$tdataequipment_uses[".showSearchPanel"] = true;


$tdataequipment_uses[".isUseAjaxSuggest"] = true;

$tdataequipment_uses[".rowHighlite"] = true;


// button handlers file names

$tdataequipment_uses[".addPageEvents"] = false;

$tdataequipment_uses[".arrKeyFields"][] = "gkey";

// use datepicker for search panel
$tdataequipment_uses[".isUseCalendarForSearch"] = true;

// use timepicker for search panel
$tdataequipment_uses[".isUseTimeForSearch"] = false;

$tdataequipment_uses[".isUseiBox"] = false;


	

	



$tdataequipment_uses[".isUseInlineJs"] = $tdataequipment_uses[".isUseInlineAdd"] || $tdataequipment_uses[".isUseInlineEdit"];

$tdataequipment_uses[".allSearchFields"] = array();

$tdataequipment_uses[".globSearchFields"][] = "gkey";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("gkey", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "gkey";	
}
$tdataequipment_uses[".globSearchFields"][] = "eq_nbr";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("eq_nbr", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "eq_nbr";	
}
$tdataequipment_uses[".globSearchFields"][] = "line_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("line_id", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "line_id";	
}
$tdataequipment_uses[".globSearchFields"][] = "depo_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("depo_id", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "depo_id";	
}
$tdataequipment_uses[".globSearchFields"][] = "shipping_agents_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("shipping_agents_id", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "shipping_agents_id";	
}
$tdataequipment_uses[".globSearchFields"][] = "shipping_agent_name";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("shipping_agent_name", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "shipping_agent_name";	
}
$tdataequipment_uses[".globSearchFields"][] = "creator";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("creator", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "creator";	
}
$tdataequipment_uses[".globSearchFields"][] = "created";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("created", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "created";	
}
$tdataequipment_uses[".globSearchFields"][] = "port_of_origin";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("port_of_origin", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "port_of_origin";	
}
$tdataequipment_uses[".globSearchFields"][] = "consignee";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("consignee", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "consignee";	
}
$tdataequipment_uses[".globSearchFields"][] = "port_of_destination";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("port_of_destination", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "port_of_destination";	
}
$tdataequipment_uses[".globSearchFields"][] = "bl_nbr";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("bl_nbr", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "bl_nbr";	
}
$tdataequipment_uses[".globSearchFields"][] = "category";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("category", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "category";	
}
$tdataequipment_uses[".globSearchFields"][] = "document_verfied";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("document_verfied", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "document_verfied";	
}
$tdataequipment_uses[".globSearchFields"][] = "survey_pos_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("survey_pos_id", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "survey_pos_id";	
}
$tdataequipment_uses[".globSearchFields"][] = "ctr_position";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("ctr_position", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "ctr_position";	
}
$tdataequipment_uses[".globSearchFields"][] = "iso_code";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("iso_code", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "iso_code";	
}
$tdataequipment_uses[".globSearchFields"][] = "eq_size";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("eq_size", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "eq_size";	
}
$tdataequipment_uses[".globSearchFields"][] = "eq_type";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("eq_type", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "eq_type";	
}
$tdataequipment_uses[".globSearchFields"][] = "shipping_line_name";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("shipping_line_name", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "shipping_line_name";	
}
$tdataequipment_uses[".globSearchFields"][] = "tare_weight_unit";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("tare_weight_unit", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "tare_weight_unit";	
}
$tdataequipment_uses[".globSearchFields"][] = "payment_release";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("payment_release", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "payment_release";	
}
$tdataequipment_uses[".globSearchFields"][] = "trx_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("trx_id", $tdataequipment_uses[".allSearchFields"]))
{
	$tdataequipment_uses[".allSearchFields"][] = "trx_id";	
}


$tdataequipment_uses[".googleLikeFields"][] = "gkey";
$tdataequipment_uses[".googleLikeFields"][] = "eq_nbr";
$tdataequipment_uses[".googleLikeFields"][] = "line_id";
$tdataequipment_uses[".googleLikeFields"][] = "depo_id";
$tdataequipment_uses[".googleLikeFields"][] = "shipping_agents_id";
$tdataequipment_uses[".googleLikeFields"][] = "shipping_agent_name";
$tdataequipment_uses[".googleLikeFields"][] = "creator";
$tdataequipment_uses[".googleLikeFields"][] = "created";
$tdataequipment_uses[".googleLikeFields"][] = "port_of_origin";
$tdataequipment_uses[".googleLikeFields"][] = "consignee";
$tdataequipment_uses[".googleLikeFields"][] = "port_of_destination";
$tdataequipment_uses[".googleLikeFields"][] = "bl_nbr";
$tdataequipment_uses[".googleLikeFields"][] = "category";
$tdataequipment_uses[".googleLikeFields"][] = "document_verfied";
$tdataequipment_uses[".googleLikeFields"][] = "survey_pos_id";
$tdataequipment_uses[".googleLikeFields"][] = "ctr_position";
$tdataequipment_uses[".googleLikeFields"][] = "iso_code";
$tdataequipment_uses[".googleLikeFields"][] = "eq_size";
$tdataequipment_uses[".googleLikeFields"][] = "eq_type";
$tdataequipment_uses[".googleLikeFields"][] = "shipping_line_name";
$tdataequipment_uses[".googleLikeFields"][] = "tare_weight_unit";
$tdataequipment_uses[".googleLikeFields"][] = "payment_release";
$tdataequipment_uses[".googleLikeFields"][] = "trx_id";



$tdataequipment_uses[".advSearchFields"][] = "gkey";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("gkey", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "gkey";	
}
$tdataequipment_uses[".advSearchFields"][] = "eq_nbr";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("eq_nbr", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "eq_nbr";	
}
$tdataequipment_uses[".advSearchFields"][] = "line_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("line_id", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "line_id";	
}
$tdataequipment_uses[".advSearchFields"][] = "depo_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("depo_id", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "depo_id";	
}
$tdataequipment_uses[".advSearchFields"][] = "shipping_agents_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("shipping_agents_id", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "shipping_agents_id";	
}
$tdataequipment_uses[".advSearchFields"][] = "shipping_agent_name";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("shipping_agent_name", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "shipping_agent_name";	
}
$tdataequipment_uses[".advSearchFields"][] = "creator";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("creator", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "creator";	
}
$tdataequipment_uses[".advSearchFields"][] = "created";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("created", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "created";	
}
$tdataequipment_uses[".advSearchFields"][] = "port_of_origin";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("port_of_origin", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "port_of_origin";	
}
$tdataequipment_uses[".advSearchFields"][] = "consignee";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("consignee", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "consignee";	
}
$tdataequipment_uses[".advSearchFields"][] = "port_of_destination";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("port_of_destination", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "port_of_destination";	
}
$tdataequipment_uses[".advSearchFields"][] = "bl_nbr";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("bl_nbr", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "bl_nbr";	
}
$tdataequipment_uses[".advSearchFields"][] = "category";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("category", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "category";	
}
$tdataequipment_uses[".advSearchFields"][] = "document_verfied";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("document_verfied", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "document_verfied";	
}
$tdataequipment_uses[".advSearchFields"][] = "survey_pos_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("survey_pos_id", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "survey_pos_id";	
}
$tdataequipment_uses[".advSearchFields"][] = "ctr_position";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("ctr_position", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "ctr_position";	
}
$tdataequipment_uses[".advSearchFields"][] = "iso_code";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("iso_code", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "iso_code";	
}
$tdataequipment_uses[".advSearchFields"][] = "eq_size";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("eq_size", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "eq_size";	
}
$tdataequipment_uses[".advSearchFields"][] = "eq_type";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("eq_type", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "eq_type";	
}
$tdataequipment_uses[".advSearchFields"][] = "shipping_line_name";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("shipping_line_name", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "shipping_line_name";	
}
$tdataequipment_uses[".advSearchFields"][] = "tare_weight_unit";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("tare_weight_unit", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "tare_weight_unit";	
}
$tdataequipment_uses[".advSearchFields"][] = "payment_release";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("payment_release", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "payment_release";	
}
$tdataequipment_uses[".advSearchFields"][] = "trx_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("trx_id", $tdataequipment_uses[".allSearchFields"])) 
{
	$tdataequipment_uses[".allSearchFields"][] = "trx_id";	
}

$tdataequipment_uses[".isTableType"] = "list";


	



// Access doesn't support subqueries from the same table as main
$tdataequipment_uses[".subQueriesSupAccess"] = true;




$tdataequipment_uses[".totalsFields"][] = array("fName"=>"eq_nbr", "totalsType"=>"COUNT", "viewFormat"=>"");

$tdataequipment_uses[".pageSize"] = 20;

$gstrOrderBy = "";
if(strlen($gstrOrderBy) && strtolower(substr($gstrOrderBy,0,8))!="order by")
	$gstrOrderBy = "order by ".$gstrOrderBy;
$tdataequipment_uses[".strOrderBy"] = $gstrOrderBy;
	
$tdataequipment_uses[".orderindexes"] = array();

$tdataequipment_uses[".sqlHead"] = "SELECT gkey,  eq_nbr,  line_id,  depo_id,  shipping_agents_id,  shipping_agent_name,  creator,  created,  port_of_origin,  consignee,  port_of_destination,  bl_nbr,  category,  document_verfied,  survey_pos_id,  ctr_position,  iso_code,  eq_size,  eq_type,  shipping_line_name,  tare_weight_unit,  payment_release,  trx_id";
$tdataequipment_uses[".sqlFrom"] = "FROM equipment_uses";
$tdataequipment_uses[".sqlWhereExpr"] = "";
$tdataequipment_uses[".sqlTail"] = "";




//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataequipment_uses[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataequipment_uses[".arrGroupsPerPage"] = $arrGPP;

	$tableKeys = array();
	$tableKeys[] = "gkey";
	$tdataequipment_uses[".Keys"] = $tableKeys;

//	gkey
	$fdata = array();
	$fdata["strName"] = "gkey";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Gkey"; 
	
		
		
	$fdata["FieldType"]= 3;
	
		$fdata["AutoInc"]=true;
	
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "gkey";
	
		$fdata["FullName"]= "gkey";
	
		$fdata["IsRequired"]=true; 
	
		
		
		
		
				$fdata["Index"]= 1;
				$fdata["EditParams"]="";
			
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
				$fdata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						$fdata["validateAs"]["basicValidate"][] = "IsRequired";
	
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["gkey"]=$fdata;
//	eq_nbr
	$fdata = array();
	$fdata["strName"] = "eq_nbr";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Eq Nbr"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "eq_nbr";
	
		$fdata["FullName"]= "eq_nbr";
	
		
		
		
		
		
				$fdata["Index"]= 2;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=12";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["eq_nbr"]=$fdata;
//	line_id
	$fdata = array();
	$fdata["strName"] = "line_id";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Line Id"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "line_id";
	
		$fdata["FullName"]= "line_id";
	
		
		
		
		
		
				$fdata["Index"]= 3;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=4";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["line_id"]=$fdata;
//	depo_id
	$fdata = array();
	$fdata["strName"] = "depo_id";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Depo Id"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "depo_id";
	
		$fdata["FullName"]= "depo_id";
	
		
		
		
		
		
				$fdata["Index"]= 4;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=5";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["depo_id"]=$fdata;
//	shipping_agents_id
	$fdata = array();
	$fdata["strName"] = "shipping_agents_id";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Shipping Agents Id"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "shipping_agents_id";
	
		$fdata["FullName"]= "shipping_agents_id";
	
		
		
		
		
		
				$fdata["Index"]= 5;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=25";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["shipping_agents_id"]=$fdata;
//	shipping_agent_name
	$fdata = array();
	$fdata["strName"] = "shipping_agent_name";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Shipping Agent Name"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "shipping_agent_name";
	
		$fdata["FullName"]= "shipping_agent_name";
	
		
		
		
		
		
				$fdata["Index"]= 6;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=75";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["shipping_agent_name"]=$fdata;
//	creator
	$fdata = array();
	$fdata["strName"] = "creator";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Creator"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "creator";
	
		$fdata["FullName"]= "creator";
	
		
		
		
		
		
				$fdata["Index"]= 7;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=8";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["creator"]=$fdata;
//	created
	$fdata = array();
	$fdata["strName"] = "created";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Created"; 
	
		
		
	$fdata["FieldType"]= 135;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Date";
	$fdata["ViewFormat"]= "Short Date";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "created";
	
		$fdata["FullName"]= "created";
	
		
		
		
		
		
				$fdata["Index"]= 8;
		$fdata["DateEditType"] = 13; 
	$fdata["InitialYearFactor"] = 100; 
	$fdata["LastYearFactor"] = 10; 
			
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["created"]=$fdata;
//	port_of_origin
	$fdata = array();
	$fdata["strName"] = "port_of_origin";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Port Of Origin"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "port_of_origin";
	
		$fdata["FullName"]= "port_of_origin";
	
		
		
		
		
		
				$fdata["Index"]= 9;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=5";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["port_of_origin"]=$fdata;
//	consignee
	$fdata = array();
	$fdata["strName"] = "consignee";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Consignee"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "consignee";
	
		$fdata["FullName"]= "consignee";
	
		
		
		
		
		
				$fdata["Index"]= 10;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=50";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["consignee"]=$fdata;
//	port_of_destination
	$fdata = array();
	$fdata["strName"] = "port_of_destination";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Port Of Destination"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "port_of_destination";
	
		$fdata["FullName"]= "port_of_destination";
	
		
		
		
		
		
				$fdata["Index"]= 11;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=5";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["port_of_destination"]=$fdata;
//	bl_nbr
	$fdata = array();
	$fdata["strName"] = "bl_nbr";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Bl Nbr"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "bl_nbr";
	
		$fdata["FullName"]= "bl_nbr";
	
		
		
		
		
		
				$fdata["Index"]= 12;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=20";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["bl_nbr"]=$fdata;
//	category
	$fdata = array();
	$fdata["strName"] = "category";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Category"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "category";
	
		$fdata["FullName"]= "category";
	
		
		
		
		
		
				$fdata["Index"]= 13;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=2";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["category"]=$fdata;
//	document_verfied
	$fdata = array();
	$fdata["strName"] = "document_verfied";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Document Verfied"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "document_verfied";
	
		$fdata["FullName"]= "document_verfied";
	
		
		
		
		
		
				$fdata["Index"]= 14;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=1";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["document_verfied"]=$fdata;
//	survey_pos_id
	$fdata = array();
	$fdata["strName"] = "survey_pos_id";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Survey Pos Id"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "survey_pos_id";
	
		$fdata["FullName"]= "survey_pos_id";
	
		
		
		
		
		
				$fdata["Index"]= 15;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=12";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["survey_pos_id"]=$fdata;
//	ctr_position
	$fdata = array();
	$fdata["strName"] = "ctr_position";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Ctr Position"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "ctr_position";
	
		$fdata["FullName"]= "ctr_position";
	
		
		
		
		
		
				$fdata["Index"]= 16;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=15";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["ctr_position"]=$fdata;
//	iso_code
	$fdata = array();
	$fdata["strName"] = "iso_code";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Iso Code"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "iso_code";
	
		$fdata["FullName"]= "iso_code";
	
		
		
		
		
		
				$fdata["Index"]= 17;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=4";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["iso_code"]=$fdata;
//	eq_size
	$fdata = array();
	$fdata["strName"] = "eq_size";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Eq Size"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "eq_size";
	
		$fdata["FullName"]= "eq_size";
	
		
		
		
		
		
				$fdata["Index"]= 18;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=2";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["eq_size"]=$fdata;
//	eq_type
	$fdata = array();
	$fdata["strName"] = "eq_type";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Eq Type"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "eq_type";
	
		$fdata["FullName"]= "eq_type";
	
		
		
		
		
		
				$fdata["Index"]= 19;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=2";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["eq_type"]=$fdata;
//	shipping_line_name
	$fdata = array();
	$fdata["strName"] = "shipping_line_name";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Shipping Line Name"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "shipping_line_name";
	
		$fdata["FullName"]= "shipping_line_name";
	
		
		
		
		
		
				$fdata["Index"]= 20;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=75";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["shipping_line_name"]=$fdata;
//	tare_weight_unit
	$fdata = array();
	$fdata["strName"] = "tare_weight_unit";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Tare Weight Unit"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "tare_weight_unit";
	
		$fdata["FullName"]= "tare_weight_unit";
	
		
		
		
		
		
				$fdata["Index"]= 21;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=3";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["tare_weight_unit"]=$fdata;
//	payment_release
	$fdata = array();
	$fdata["strName"] = "payment_release";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Payment Release"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "payment_release";
	
		$fdata["FullName"]= "payment_release";
	
		
		
		
		
		
				$fdata["Index"]= 22;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=1";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["payment_release"]=$fdata;
//	trx_id
	$fdata = array();
	$fdata["strName"] = "trx_id";
	$fdata["ownerTable"] = "equipment_uses";
	$fdata["Label"]="Trx Id"; 
	
		
		
	$fdata["FieldType"]= 3;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "trx_id";
	
		$fdata["FullName"]= "trx_id";
	
		
		
		
		
		
				$fdata["Index"]= 23;
				$fdata["EditParams"]="";
			
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
				$fdata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdataequipment_uses["trx_id"]=$fdata;


	
$tables_data["equipment_uses"]=&$tdataequipment_uses;
$field_labels["equipment_uses"] = &$fieldLabelsequipment_uses;
$fieldToolTips["equipment_uses"] = &$fieldToolTipsequipment_uses;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["equipment_uses"] = array();

	
// tables which are master tables for current table (detail)
$masterTablesData["equipment_uses"] = array();

$mIndex = 1-1;
			$strOriginalDetailsTable="transaction";
	$masterTablesData["equipment_uses"][$mIndex] = array(
		  "mDataSourceTable"=>"transaction"
		, "mOriginalTable" => $strOriginalDetailsTable
		, "mShortTable" => "transaction"
		, "masterKeys" => array()
		, "detailKeys" => array()
		, "dispChildCount" => "1"
		, "hideChild" => "0"	
		, "dispInfo" => "1"								
		, "previewOnList" => 0
		, "previewOnAdd" => 0
		, "previewOnEdit" => 0
		, "previewOnView" => 1
	);	
		$masterTablesData["equipment_uses"][$mIndex]["masterKeys"][]="trx_id";
		$masterTablesData["equipment_uses"][$mIndex]["detailKeys"][]="trx_id";

// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_equipment_uses()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "gkey,  eq_nbr,  line_id,  depo_id,  shipping_agents_id,  shipping_agent_name,  creator,  created,  port_of_origin,  consignee,  port_of_destination,  bl_nbr,  category,  document_verfied,  survey_pos_id,  ctr_position,  iso_code,  eq_size,  eq_type,  shipping_line_name,  tare_weight_unit,  payment_release,  trx_id";
$proto0["m_strFrom"] = "FROM equipment_uses";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = "0";
$proto1["m_inBrackets"] = "0";
$proto1["m_useAlias"] = "0";
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = "0";
$proto3["m_inBrackets"] = "0";
$proto3["m_useAlias"] = "0";
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "gkey",
	"m_strTable" => "equipment_uses"
));

$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "eq_nbr",
	"m_strTable" => "equipment_uses"
));

$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "line_id",
	"m_strTable" => "equipment_uses"
));

$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto0["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "depo_id",
	"m_strTable" => "equipment_uses"
));

$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto0["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "shipping_agents_id",
	"m_strTable" => "equipment_uses"
));

$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto0["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "shipping_agent_name",
	"m_strTable" => "equipment_uses"
));

$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto0["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "creator",
	"m_strTable" => "equipment_uses"
));

$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto0["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "created",
	"m_strTable" => "equipment_uses"
));

$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto0["m_fieldlist"][]=$obj;
						$proto21=array();
			$obj = new SQLField(array(
	"m_strName" => "port_of_origin",
	"m_strTable" => "equipment_uses"
));

$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "";
$obj = new SQLFieldListItem($proto21);

$proto0["m_fieldlist"][]=$obj;
						$proto23=array();
			$obj = new SQLField(array(
	"m_strName" => "consignee",
	"m_strTable" => "equipment_uses"
));

$proto23["m_expr"]=$obj;
$proto23["m_alias"] = "";
$obj = new SQLFieldListItem($proto23);

$proto0["m_fieldlist"][]=$obj;
						$proto25=array();
			$obj = new SQLField(array(
	"m_strName" => "port_of_destination",
	"m_strTable" => "equipment_uses"
));

$proto25["m_expr"]=$obj;
$proto25["m_alias"] = "";
$obj = new SQLFieldListItem($proto25);

$proto0["m_fieldlist"][]=$obj;
						$proto27=array();
			$obj = new SQLField(array(
	"m_strName" => "bl_nbr",
	"m_strTable" => "equipment_uses"
));

$proto27["m_expr"]=$obj;
$proto27["m_alias"] = "";
$obj = new SQLFieldListItem($proto27);

$proto0["m_fieldlist"][]=$obj;
						$proto29=array();
			$obj = new SQLField(array(
	"m_strName" => "category",
	"m_strTable" => "equipment_uses"
));

$proto29["m_expr"]=$obj;
$proto29["m_alias"] = "";
$obj = new SQLFieldListItem($proto29);

$proto0["m_fieldlist"][]=$obj;
						$proto31=array();
			$obj = new SQLField(array(
	"m_strName" => "document_verfied",
	"m_strTable" => "equipment_uses"
));

$proto31["m_expr"]=$obj;
$proto31["m_alias"] = "";
$obj = new SQLFieldListItem($proto31);

$proto0["m_fieldlist"][]=$obj;
						$proto33=array();
			$obj = new SQLField(array(
	"m_strName" => "survey_pos_id",
	"m_strTable" => "equipment_uses"
));

$proto33["m_expr"]=$obj;
$proto33["m_alias"] = "";
$obj = new SQLFieldListItem($proto33);

$proto0["m_fieldlist"][]=$obj;
						$proto35=array();
			$obj = new SQLField(array(
	"m_strName" => "ctr_position",
	"m_strTable" => "equipment_uses"
));

$proto35["m_expr"]=$obj;
$proto35["m_alias"] = "";
$obj = new SQLFieldListItem($proto35);

$proto0["m_fieldlist"][]=$obj;
						$proto37=array();
			$obj = new SQLField(array(
	"m_strName" => "iso_code",
	"m_strTable" => "equipment_uses"
));

$proto37["m_expr"]=$obj;
$proto37["m_alias"] = "";
$obj = new SQLFieldListItem($proto37);

$proto0["m_fieldlist"][]=$obj;
						$proto39=array();
			$obj = new SQLField(array(
	"m_strName" => "eq_size",
	"m_strTable" => "equipment_uses"
));

$proto39["m_expr"]=$obj;
$proto39["m_alias"] = "";
$obj = new SQLFieldListItem($proto39);

$proto0["m_fieldlist"][]=$obj;
						$proto41=array();
			$obj = new SQLField(array(
	"m_strName" => "eq_type",
	"m_strTable" => "equipment_uses"
));

$proto41["m_expr"]=$obj;
$proto41["m_alias"] = "";
$obj = new SQLFieldListItem($proto41);

$proto0["m_fieldlist"][]=$obj;
						$proto43=array();
			$obj = new SQLField(array(
	"m_strName" => "shipping_line_name",
	"m_strTable" => "equipment_uses"
));

$proto43["m_expr"]=$obj;
$proto43["m_alias"] = "";
$obj = new SQLFieldListItem($proto43);

$proto0["m_fieldlist"][]=$obj;
						$proto45=array();
			$obj = new SQLField(array(
	"m_strName" => "tare_weight_unit",
	"m_strTable" => "equipment_uses"
));

$proto45["m_expr"]=$obj;
$proto45["m_alias"] = "";
$obj = new SQLFieldListItem($proto45);

$proto0["m_fieldlist"][]=$obj;
						$proto47=array();
			$obj = new SQLField(array(
	"m_strName" => "payment_release",
	"m_strTable" => "equipment_uses"
));

$proto47["m_expr"]=$obj;
$proto47["m_alias"] = "";
$obj = new SQLFieldListItem($proto47);

$proto0["m_fieldlist"][]=$obj;
						$proto49=array();
			$obj = new SQLField(array(
	"m_strName" => "trx_id",
	"m_strTable" => "equipment_uses"
));

$proto49["m_expr"]=$obj;
$proto49["m_alias"] = "";
$obj = new SQLFieldListItem($proto49);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto51=array();
$proto51["m_link"] = "SQLL_MAIN";
			$proto52=array();
$proto52["m_strName"] = "equipment_uses";
$proto52["m_columns"] = array();
$proto52["m_columns"][] = "gkey";
$proto52["m_columns"][] = "eq_nbr";
$proto52["m_columns"][] = "line_id";
$proto52["m_columns"][] = "depo_id";
$proto52["m_columns"][] = "shipping_agents_id";
$proto52["m_columns"][] = "shipping_agent_name";
$proto52["m_columns"][] = "in_visit_type_id";
$proto52["m_columns"][] = "in_time";
$proto52["m_columns"][] = "out_time";
$proto52["m_columns"][] = "eq_condition_ingate_check";
$proto52["m_columns"][] = "eq_condition_yard_check";
$proto52["m_columns"][] = "eq_condition_outgate_check";
$proto52["m_columns"][] = "sci_seal_number";
$proto52["m_columns"][] = "shipper";
$proto52["m_columns"][] = "creator";
$proto52["m_columns"][] = "created";
$proto52["m_columns"][] = "changer";
$proto52["m_columns"][] = "changed";
$proto52["m_columns"][] = "port_of_origin";
$proto52["m_columns"][] = "consignee";
$proto52["m_columns"][] = "in_truck_license_nbr";
$proto52["m_columns"][] = "in_trucking_company_group_id";
$proto52["m_columns"][] = "in_trucking_company_name";
$proto52["m_columns"][] = "in_driver_name";
$proto52["m_columns"][] = "port_of_destination";
$proto52["m_columns"][] = "depo_name";
$proto52["m_columns"][] = "out_truck_license_nbr";
$proto52["m_columns"][] = "out_trucking_company_group_id";
$proto52["m_columns"][] = "out_trucking_company_name";
$proto52["m_columns"][] = "out_driver_name";
$proto52["m_columns"][] = "out_visit_type_id";
$proto52["m_columns"][] = "out_port_of_origin";
$proto52["m_columns"][] = "out_port_of_destination";
$proto52["m_columns"][] = "bl_nbr";
$proto52["m_columns"][] = "eq_damage";
$proto52["m_columns"][] = "status";
$proto52["m_columns"][] = "category";
$proto52["m_columns"][] = "document_verfied";
$proto52["m_columns"][] = "survey_pos_id";
$proto52["m_columns"][] = "ctr_position";
$proto52["m_columns"][] = "iso_code";
$proto52["m_columns"][] = "eq_size";
$proto52["m_columns"][] = "eq_type";
$proto52["m_columns"][] = "shipping_line_name";
$proto52["m_columns"][] = "damage_status";
$proto52["m_columns"][] = "wash_water_service_flag";
$proto52["m_columns"][] = "wash_deter_service_flag";
$proto52["m_columns"][] = "wash_chemi_service_flag";
$proto52["m_columns"][] = "repair_service_flag";
$proto52["m_columns"][] = "out_release_status";
$proto52["m_columns"][] = "out_doc_verified";
$proto52["m_columns"][] = "shipper_name";
$proto52["m_columns"][] = "out_damage_status";
$proto52["m_columns"][] = "ready_to_print_eir";
$proto52["m_columns"][] = "print_eir_complete";
$proto52["m_columns"][] = "ready_to_print_eir_out";
$proto52["m_columns"][] = "print_eir_out_complete";
$proto52["m_columns"][] = "sci_seal_filename";
$proto52["m_columns"][] = "seal_plug_check";
$proto52["m_columns"][] = "trx_id";
$proto52["m_columns"][] = "tare_weight_unit";
$proto52["m_columns"][] = "payment_release";
$proto52["m_columns"][] = "out_payment_release";
$proto52["m_columns"][] = "tare_weight";
$proto52["m_columns"][] = "trx_ido";
$proto52["m_columns"][] = "csc_nbr";
$proto52["m_columns"][] = "max_gross_weight";
$proto52["m_columns"][] = "allow_stack";
$proto52["m_columns"][] = "racking_test_load";
$proto52["m_columns"][] = "date_manufactured";
$obj = new SQLTable($proto52);

$proto51["m_table"] = $obj;
$proto51["m_alias"] = "";
$proto53=array();
$proto53["m_sql"] = "";
$proto53["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto53["m_column"]=$obj;
$proto53["m_contained"] = array();
$proto53["m_strCase"] = "";
$proto53["m_havingmode"] = "0";
$proto53["m_inBrackets"] = "0";
$proto53["m_useAlias"] = "0";
$obj = new SQLLogicalExpr($proto53);

$proto51["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto51);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$obj = new SQLQuery($proto0);

return $obj;
}
$queryData_equipment_uses = createSqlQuery_equipment_uses();
$tdataequipment_uses[".sqlquery"] = $queryData_equipment_uses;



$tableEvents["equipment_uses"] = new eventsBase;
$tdataequipment_uses[".hasEvents"] = false;

?>
