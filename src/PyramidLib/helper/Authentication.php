<?php

namespace PyramidLib\helper;

class Authentication {

    protected $secretKey;
    protected $methodCrip;

    public function __construct($secretKey)
    {
        $this->secretKey = $secretKey;
        $this->methodCrip = mcrypt_module_open('rijndael-256', '', 'ecb', '');
    }

    public function encrypt_rijndael_256($string)
    {
        /* Open the cipher */
        $td = $this->methodCrip;

        /* Create the IV and determine the keysize length, use MCRYPT_RAND
         * on Windows instead */
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_DEV_RANDOM);
        $ks = mcrypt_enc_get_key_size($td);

        /* Create key */
        $key = substr(md5($this->secretKey), 0, $ks);

        /* Intialize encryption */
        mcrypt_generic_init($td, $key, $iv);

        /* Encrypt data */
        $encrypted = mcrypt_generic($td, $string);

        /* Base64_encode */
        $result = base64_encode($encrypted);

        /* Return string Encrip */;
        return $result;
    }

    public function decrypt_rijndael_256($string)
    {
        /* Open the cipher */
        $td = $this->methodCrip;

        /* Create the IV and determine the keysize length, use MCRYPT_RAND
         * on Windows instead */
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_DEV_RANDOM);
        $ks = mcrypt_enc_get_key_size($td);

        /* Create key */
        $key = substr(md5($this->secretKey), 0, $ks);

        /* Initialize encryption module for decryption */
        mcrypt_generic_init($td, $key, $iv);

        /* Decrypt encrypted string */
        $decrypted = mdecrypt_generic($td, base64_decode($string));

        return trim($decrypted);
    }

    public function close()
    {
        /* Open the cipher */
        $td = $this->methodCrip;

        /* Terminate decryption handle and close module */
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
    }

}
